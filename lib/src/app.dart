import 'package:flutter/material.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/generate_route.dart';
import 'package:techbusinessfrontend/src/utils/token_helper.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var token = TokenHelper();

    return MaterialApp(
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: ConstantsColors.primaryColor,
          accentColor: ConstantsColors.secondaryColor,
          iconTheme: IconThemeData(color: ConstantsColors.darkGray, size: 48),
          buttonColor: ConstantsColors.primaryColor,
          fontFamily: 'CenturyGothic',
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold),
            headline2: TextStyle(fontSize: 26.0, fontWeight: FontWeight.normal),
            headline3: TextStyle(fontSize: 22.0, fontStyle: FontStyle.italic),
            bodyText1: TextStyle(fontSize: 20.0, fontFamily: 'Hind'),
            bodyText2: TextStyle(fontSize: 16.0, fontFamily: 'Hind'),
          ),
        ),
        debugShowCheckedModeBanner: false,
        title: Constants.appName,
        initialRoute: token.existToken()
            ? _switchUiByRole(context, token)
            : Constants.authUiRoute,
        onGenerateRoute: RouterCustom.onGenerateRoute);
  }

  String _switchUiByRole(BuildContext context, TokenHelper token) {
    token.getToken();
    var payload = Jwt.parseJwt(token.accessToken);
    if (payload['ROLES'][0] == 'ROLE_EMPLOYEE') {
      return Constants.employeeHomeUiRoute;
    } else if (payload['ROLES'][0] == 'ROLE_ADMIN') {
      return Constants.administratorHomeRoute;
    }
    return null;
  }
}
