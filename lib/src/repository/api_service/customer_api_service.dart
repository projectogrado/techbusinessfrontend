import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/customer_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class CustomerApiService {
  ApiResponse _apiResponse;
  Customer _customer;
  ErrorApiResponse _error;

  CustomerApiService();

  Future<ApiResponse> getAll(String accessToken) async {
    List<Customer> listCustomer;
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    Uri url;
    url = Uri.http(Constants.urlAuthority, Constants.urlCustomer + '/all');
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader:
          Constants.authorizationHeader + accessToken,
    });
    var resBody = json.decode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    listCustomer = [];
    if (apiResponse.statusResponse == HttpStatus.ok) {
      resBody.forEach((process) {
        listCustomer.add(Customer.fromJson(process));
      });
      apiResponse.payload = listCustomer;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }

  Future<ApiResponse> register(Customer customer, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(customer.toJsonRegistry());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlCustomer + '/insert-customer');
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _customer = Customer.fromJson(resBody);
      _apiResponse.payload = _customer;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> getCustomerByDocument(
      String document, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlCustomer + '/' + document);

    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: 'Bearer ' + token,
    });

    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;

    if (_apiResponse.statusResponse == 200) {
      _customer = Customer.fromIgnoreList(resBody);
      _apiResponse.payload = _customer;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Customer customer, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(customer.toJson());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlCustomer + '/update-customer');
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _customer = Customer.fromJson(resBody);
      _apiResponse.payload = _customer;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }
}
