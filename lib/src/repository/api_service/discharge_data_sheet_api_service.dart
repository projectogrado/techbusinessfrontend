import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/discharge_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class DischargeDataSheetApiService {
  ApiResponse _apiResponse;
  DischargeDataSheet _dischargeDataSheet;
  ErrorApiResponse _error;

  DischargeDataSheetApiService();

  Future<ApiResponse> getDischargeDataSheetById(String token, String id) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var pathVariable = '/' + id;
    var uri =
        Uri.http(Constants.urlAuthority, Constants.urlDischarge + pathVariable);

    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: 'Bearer ' + token,
    });

    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;

    if (_apiResponse.statusResponse == 200) {
      _dischargeDataSheet = DischargeDataSheet.fromJson(resBody);
      _apiResponse.payload = _dischargeDataSheet;
      return _apiResponse;
    }

    _error = ErrorApiResponse.fromJson(resBody);
    _apiResponse.payload = _error;
    return _apiResponse;
  }
}
