import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class ProcessApiService {
  ApiResponse _apiResponse;
  Process _process;
  ErrorApiResponse _error;

  ProcessApiService();

  Future<ApiResponse> create(Process process, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(process.toJsonRegistry());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlProcess + '/insert-process');
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _process = Process.fromJson(resBody);
      _apiResponse.payload = _process;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Process process, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(process.toJson());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlProcess + '/update-process');
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _process = Process.fromJson(resBody);
      _apiResponse.payload = _process;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> getAll(String accessToken) async {
    List<Process> listProcess;
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);
    Uri url;
    url = Uri.http(Constants.urlAuthority, Constants.urlProcess + '/all');
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader:
          Constants.authorizationHeader + accessToken,
    });
    var resBody = json.decode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    listProcess = [];
    if (apiResponse.statusResponse == HttpStatus.ok) {
      resBody.forEach((process) {
        listProcess.add(Process.fromJson(process));
      });
      apiResponse.payload = listProcess;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }
}
