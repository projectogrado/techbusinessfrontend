import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/person_receive_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class PersonReceiveApiService {
  ApiResponse _apiResponse;
  PersonReceive _personReceive;
  ErrorApiResponse _error;

  PersonReceiveApiService();

  Future<ApiResponse> register(
      PersonReceive personReceive, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(personReceive.toJsonRegistry());
    var uri = Uri.http(Constants.urlAuthority,
        Constants.urlPersonReceive + '/insert-person-receive');
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.payload = _personReceive;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }
}
