import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/checklist_repair_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class WorkServiceApiService {
  ApiResponse _apiResponse;
  WorkService _workService;
  String _message;
  ErrorApiResponse _error;

  WorkServiceApiService();

  Future<ApiResponse> register(WorkService workService, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(workService.toJsonRegistry());
    var uri = Uri.http(Constants.urlAuthority,
        Constants.urlWorkService + '/insert-work-service');
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _workService = WorkService.fromJson(resBody);
      _apiResponse.payload = _workService;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> addCheckListWorkService(
      List<ChecklistRepair> list, int id, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var listJson = [];
    list.forEach((element) {
      listJson.add(element.toJson());
    });
    var body = json.encode(listJson);
    var queryParameters = <String, dynamic>{
      'id': id.toString(),
    };
    var uri = Uri.http(
        Constants.urlAuthority,
        Constants.urlWorkService + '/add-checklist-work-service',
        queryParameters);
    var res = await http.patch(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.payload = _message;
    } else {
    }
    return _apiResponse;
  }

  Future<ApiResponse> getAllByAdmission(int id,String token) async {
    List<WorkService> list;
    _apiResponse = ApiResponse(statusResponse: 0);
    var uri = Uri.http(Constants.urlAuthority, Constants.urlWorkService + '/all/admission/'+id.toString());

    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: 'Bearer ' + token,
    });

    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    list = [];

    if (_apiResponse.statusResponse == 200) {
      resBody.forEach((workService) {
        list.add(WorkService.fromJson(workService));
        return workService;
      });
      _apiResponse.payload = list;
      return _apiResponse;
    }

    _error = ErrorApiResponse.fromJson(resBody);
    _apiResponse.payload = _error;
    return _apiResponse;
  }

}
