import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class EmployeeApiService {
  ApiResponse _apiResponse;
  Employee _employee;
  ErrorApiResponse _error;

  EmployeeApiService();

  Future<ApiResponse> register(Employee employee, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    employee.status = true;
    var body = json.encode(employee.toJsonRegistry());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlEmployee + '/insert-employee');
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _employee = Employee.fromJson(resBody);
      _apiResponse.payload = _employee;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Employee employee, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(employee.toJson());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlEmployee + '/update-employee');
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _employee = Employee.fromJson(resBody);
      _apiResponse.payload = _employee;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> getAll(String token) async {
    List<Employee> listEmployee;
    _apiResponse = ApiResponse(statusResponse: 0);
    var uri = Uri.http(Constants.urlAuthority, Constants.urlEmployee + '/all');

    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: 'Bearer ' + token,
    });

    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    listEmployee = [];

    if (_apiResponse.statusResponse == 200) {
      resBody.forEach((employee) {
        listEmployee.add(Employee.fromJson(employee));
        return employee;
      });
      _apiResponse.payload = listEmployee;
      return _apiResponse;
    }

    _error = ErrorApiResponse.fromJson(resBody);
    _apiResponse.payload = _error;
    return _apiResponse;
  }

  Future<ApiResponse> getEmployeeLogged(String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var uri = Uri.http(Constants.urlAuthority, Constants.urlEmployee + '/me');

    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: 'Bearer ' + token,
    });

    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;

    if (_apiResponse.statusResponse == 200) {
      var employee = Employee.fromJson(resBody);

      _apiResponse.payload = employee;
      return _apiResponse;
    }

    _error = ErrorApiResponse.fromJson(resBody);
    _apiResponse.payload = _error;
    return _apiResponse;
  }
}
