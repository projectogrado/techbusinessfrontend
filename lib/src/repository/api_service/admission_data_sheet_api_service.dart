import 'dart:convert';
import 'dart:io';

import 'package:ext_storage/ext_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class AdmissionDataSheetApiService {
  ApiResponse _apiResponse;
  AdmissionDataSheet _admissionDataSheet;
  ErrorApiResponse _error;

  AdmissionDataSheetApiService();

  Future<ApiResponse> register(
      AdmissionDataSheet admissionDataSheet, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(admissionDataSheet.toJsonRegistry());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlAdmission + '/insert-admission');
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _admissionDataSheet = AdmissionDataSheet.fromJson(resBody);
      _apiResponse.payload = _admissionDataSheet;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> getById(int id, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var uri = Uri.http(Constants.urlAuthority,
        Constants.urlAdmission + '/public/' + id.toString());
    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: Constants.authorizationHeader + token,
    });
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _admissionDataSheet = AdmissionDataSheet.fromJson(resBody);
      _apiResponse.payload = _admissionDataSheet;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> climbAdmission(int id, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var params = <String, dynamic>{'admission': id.toString()};
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlAdmission + '/climb-job', params);
    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: Constants.authorizationHeader + token,
    });
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> updateStatus(int id, String status, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var params = <String, dynamic>{'id': id.toString(), 'status': status};
    var uri = Uri.http(Constants.urlAuthority,
        Constants.urlAdmission + '/update-status-admission', params);
    var res = await http.patch(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: Constants.authorizationHeader + token,
    });
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse = ApiResponse.fromJson(resBody);
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> getPdfById(int id, String token) async {
    var path = await ExtStorage.getExternalStoragePublicDirectory(
        ExtStorage.DIRECTORY_DOCUMENTS);

    try {
      _apiResponse = ApiResponse(statusResponse: 0);
      var uri = Uri.http(Constants.urlAuthority,
          Constants.urlAdmission + '/generate-pdf/' + id.toString());
      var res = await http.get(uri, headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
        HttpHeaders.authorizationHeader: Constants.authorizationHeader + token,
      });
      _apiResponse.statusResponse = res.statusCode;
      if (res.statusCode == HttpStatus.ok) {
        var bytes = base64.decode(res.body);
        final formatter = DateFormat('yyyy-MM-dd-HH-mm-ss');
        var name = 'admission_' + formatter.format(DateTime.now()) + '.pdf';
        var file = File('$path/' + name);
        var raf = file.openSync(mode: FileMode.write);
        raf.writeFromSync(bytes);
        await raf.close();
      }
      // ignore: empty_catches
    } catch (e) {}
    return _apiResponse;
  }

  Future<ApiResponse> sendForMail(int id, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var uri = Uri.http(Constants.urlAuthority,
        Constants.urlAdmission + '/send-pdf/' + id.toString());
    var res = await http.get(uri, headers: {
      HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      HttpHeaders.authorizationHeader: Constants.authorizationHeader + token,
    });
    var resBody = json.decode(utf8.decode(res.bodyBytes));

    _apiResponse.statusResponse = res.statusCode;
    if (res.statusCode == HttpStatus.ok) {
      _apiResponse = ApiResponse.fromJson(resBody);
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }
}
