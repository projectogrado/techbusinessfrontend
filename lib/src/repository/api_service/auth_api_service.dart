import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/login_request_model.dart';
import 'package:techbusinessfrontend/src/model/recover_password.dart';
import 'package:techbusinessfrontend/src/model/success_login_model.dart';
import 'package:techbusinessfrontend/src/model/user_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class AuthApiService {
  ApiResponse _apiResponse;
  SuccessLogin _successLogin;
  User _user;
  ErrorApiResponse _error;

  AuthApiService();

  Future<ApiResponse> login(LoginRequest loginRequest) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(loginRequest);
    var uri = Uri.http(Constants.urlAuthority, Constants.urlLogin);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _successLogin = SuccessLogin.fromJson(resBody);
      _apiResponse.payload = _successLogin;
    } else if (_apiResponse.statusResponse == HttpStatus.permanentRedirect) {
      _apiResponse = ApiResponse.fromJson(resBody);
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> signUp(User user) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(user);
    var uri = Uri.http(Constants.urlAuthority, Constants.urlRegisterUser);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _user = User.fromJson(resBody);
      _apiResponse.payload = _user;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> resetPassword(String username) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var params = <String, dynamic>{'username': username};
    var uri = Uri.http(Constants.urlAuthority, Constants.urlResetPassword+'recover-password', params);
    var res = await http.post(uri, headers: {HttpHeaders.contentTypeHeader: Constants.contentTypeHeader});
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
    } else {
      var resBody = json.decode(utf8.decode(res.bodyBytes));
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> updatePassword(RecoverPassword recoverPassword) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(recoverPassword.toJson());

    var uri = Uri.http(Constants.urlAuthority, Constants.urlResetPassword+'/update-password');
    var res = await http.put(uri, headers: {HttpHeaders.contentTypeHeader: Constants.contentTypeHeader}, body: body);
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      var loginRequest = LoginRequest();
      loginRequest.username = recoverPassword.username;
      loginRequest.password = recoverPassword.password;
      return login(loginRequest);
    } else {
      var resBody = json.decode(utf8.decode(res.bodyBytes));
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }
}
