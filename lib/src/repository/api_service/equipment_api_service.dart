import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class EquipmentApiService {
  ApiResponse _apiResponse;
  Equipment _equipment;
  ErrorApiResponse _error;
  var queryParameters;

  EquipmentApiService();

  Future<ApiResponse> create(Equipment equipment, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(equipment.toJsonRegistry());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlEquipment + '/insert-equipment');
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _equipment = Equipment.fromJson(resBody);
      _apiResponse.payload = _equipment;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Equipment equipment, String token) async {
    _apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(equipment.toJson());
    var uri = Uri.http(
        Constants.urlAuthority, Constants.urlEquipment + '/update-equipment');
    var res = await http.put(uri,
        headers: {
          HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
          HttpHeaders.authorizationHeader:
              Constants.authorizationHeader + token,
        },
        body: body);
    var resBody = json.decode(utf8.decode(res.bodyBytes));
    _apiResponse.statusResponse = res.statusCode;
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _equipment = Equipment.fromJson(resBody);
      _apiResponse.payload = _equipment;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      _apiResponse.payload = _error;
    }
    return _apiResponse;
  }

  Future<ApiResponse> getAll(String accessToken, var option) async {
    List<Equipment> listEquipment;
    ApiResponse apiResponse;
    apiResponse = ApiResponse(statusResponse: 0);

    if (option['id'] != null) {
      queryParameters = {'id': option['id']};
    }
    if (option['mark'] != null) {
      queryParameters = {'mark': option['mark']};
    }
    if (option['option'] == 'a') {
      queryParameters = {
        'sort': 'mark',
      };
    }
    if (option['option'] == 'b') {
      queryParameters = {
        'sort': 'id',
      };
    }
    Uri url;
    url = Uri.http(Constants.urlAuthority, Constants.urlEquipment + '/all',
        queryParameters);
    var res = await http.get(url, headers: {
      HttpHeaders.authorizationHeader:
          Constants.authorizationHeader + accessToken,
    });
    var resBody = json.decode(utf8.decode(res.bodyBytes));

    apiResponse.statusResponse = res.statusCode;

    listEquipment = [];
    if (apiResponse.statusResponse == HttpStatus.ok) {
      resBody.forEach((equipment) {
        listEquipment.add(Equipment.fromJson(equipment));
      });
      apiResponse.payload = listEquipment;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.payload = _error;
    }

    return apiResponse;
  }
}
