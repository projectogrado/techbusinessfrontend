import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/checklist_repair_model.dart';
import 'package:techbusinessfrontend/src/model/customer_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/login_request_model.dart';
import 'package:techbusinessfrontend/src/model/person_receive_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/model/recover_password.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';
import 'package:techbusinessfrontend/src/model/user_model.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';
import 'package:techbusinessfrontend/src/repository/api_service/admission_data_sheet_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/auth_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/customer_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/discharge_data_sheet_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/employee_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/equipment_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/person_receive_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/process_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/service_api_service.dart';
import 'package:techbusinessfrontend/src/repository/api_service/work_service_api_service.dart';
import 'package:techbusinessfrontend/src/utils/token_helper.dart';

class GeneralRepository with TokenHelper {
  final authApiService = AuthApiService();
  final employeeApiService = EmployeeApiService();
  final customerApiService = CustomerApiService();
  final processApiService = ProcessApiService();
  final personReceiveApiService = PersonReceiveApiService();
  final serviceApiService = ServiceApiService();
  final workServiceApiService = WorkServiceApiService();
  final equipmentApiService = EquipmentApiService();
  final dischargeDataSheetApiService = DischargeDataSheetApiService();
  final admissionDataSheetApiService = AdmissionDataSheetApiService();

  //Discharge data sheet
  Future<ApiResponse> getDischargeDataSheetById(String id) =>
      dischargeDataSheetApiService.getDischargeDataSheetById(accessToken, id);

  //Delivery
  Future<ApiResponse> registerPersonReceive(PersonReceive personReceive) =>
      personReceiveApiService.register(personReceive, accessToken);

  //Auth
  Future<ApiResponse> login(LoginRequest loginRequest) =>
      authApiService.login(loginRequest);

  Future<ApiResponse> signUp(User user) => authApiService.signUp(user);

  Future<ApiResponse> resetPassword(String username) =>
      authApiService.resetPassword(username);

  Future<ApiResponse> updatePassword(RecoverPassword recoverPassword) =>
      authApiService.updatePassword(recoverPassword);

  //Employee
  Future<ApiResponse> register(Employee employee) =>
      employeeApiService.register(employee, accessToken);

  Future<ApiResponse> updateEmployee(Employee employee) =>
      employeeApiService.update(employee, accessToken);

  Future<ApiResponse> listEmployee() => employeeApiService.getAll(accessToken);

  Future<ApiResponse> getEmployeeLogged() =>
      employeeApiService.getEmployeeLogged(accessToken);

  //Customer
  Future<ApiResponse> getAllCustomer() =>
      customerApiService.getAll(accessToken);

  Future<ApiResponse> registerCustomer(Customer customer) =>
      customerApiService.register(customer, accessToken);

  Future<ApiResponse> updateCustomer(Customer customer) =>
      customerApiService.update(customer, accessToken);

  Future<ApiResponse> getCustomerByDocument(String document) =>
      customerApiService.getCustomerByDocument(document, accessToken);

  //Process
  Future<ApiResponse> getAllProcess() => processApiService.getAll(accessToken);

  Future<ApiResponse> createProcess(Process process) =>
      processApiService.create(process, accessToken);

  Future<ApiResponse> updateProcess(Process process) =>
      processApiService.update(process, accessToken);

  //Service
  Future<ApiResponse> getAllService() => serviceApiService.getAll(accessToken);

  Future<ApiResponse> createService(Service service) =>
      serviceApiService.create(service, accessToken);

  Future<ApiResponse> updateService(Service service) =>
      serviceApiService.update(service, accessToken);

  Future<ApiResponse> deleteService(int id) =>
      serviceApiService.delete(id, accessToken);

  //Equipment

  Future<ApiResponse> getAllEquipment(var option) =>
      equipmentApiService.getAll(accessToken, option);

  Future<ApiResponse> createEquipment(Equipment equipment) =>
      equipmentApiService.create(equipment, accessToken);

  Future<ApiResponse> updateEquipment(Equipment equipment) =>
      equipmentApiService.update(equipment, accessToken);

  //Admission
  Future<ApiResponse> createAdmission(AdmissionDataSheet admission) =>
      admissionDataSheetApiService.register(admission, accessToken);

  Future<ApiResponse> getAdmissionById(int id) =>
      admissionDataSheetApiService.getById(id, accessToken);

  Future<ApiResponse> climbAdmission(int id) =>
      admissionDataSheetApiService.climbAdmission(id, accessToken);

  Future<ApiResponse> updateStatusAdmission(int id, String status) =>
      admissionDataSheetApiService.updateStatus(id,status, accessToken);

  Future<ApiResponse> getPdfById(int id) =>
      admissionDataSheetApiService.getPdfById(id, accessToken);

  Future<ApiResponse> sendForMail(int id) =>
      admissionDataSheetApiService.sendForMail(id, accessToken);

  //WorkService
  Future<ApiResponse> createWorkService(WorkService workService) =>
      workServiceApiService.register(workService, accessToken);

  Future<ApiResponse> addCheckListWorkService(
          List<ChecklistRepair> list, int id) =>
      workServiceApiService.addCheckListWorkService(list, id, accessToken);

  Future<ApiResponse> getAllWorkByAdmission(int id) => workServiceApiService.getAllByAdmission(id,accessToken);
}
