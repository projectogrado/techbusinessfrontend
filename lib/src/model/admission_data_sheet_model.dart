import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';

import 'customer_model.dart';

class AdmissionDataSheet {
  int id;
  String date;
  String reason;
  String observation;
  List<WorkService> workServices;
  Customer customer;
  Equipment equipment;

  AdmissionDataSheet(
      {this.id,
      this.date,
      this.reason,
      this.observation,
      this.workServices,
      this.customer,
      this.equipment});

  factory AdmissionDataSheet.fromJson(Map<String, dynamic> parsedJson) {
    var works = parsedJson['workServices'] as List;

    var workService = works != null
        ? works.map((i) => WorkService.fromJson(i)).toList()
        : List<WorkService>.empty();

    return AdmissionDataSheet(
        id: parsedJson['id'],
        date: parsedJson['date'],
        observation: parsedJson['observation'],
        reason: parsedJson['reason'],
        workServices: workService,
        customer: Customer.fromJson(parsedJson['customer']),
        equipment: Equipment.fromJson(parsedJson['equipment']));
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'date': date,
        'observation': observation,
        'reason': reason,
        'workServices': workServices != null
            ? workServices.map((i) => i.toJson()).toList()
            : List.empty(),
        'customer': customer,
        'equipment': equipment
      };

  Map<String, dynamic> toJsonRegistry() => {
        'date': date,
        'observation': observation,
        'reason': reason,
        'workServices': workServices != null
            ? workServices.map((i) => i.toJson()).toList()
            : List.empty(),
        'customer': customer,
        'equipment': equipment
      };

  Map<String, dynamic> toJsonRegistryList() => {
    'id': id,
    'date': date,
    'observation': observation,
    'reason': reason,
    'customer': customer,
    'equipment': equipment
  };
}
