class RecoverPassword {
  String password;
  String verifiedPassword;
  String oldPassword;
  String role;
  String username;

  RecoverPassword(
      {this.password,
        this.verifiedPassword,
        this.oldPassword,
        this.role,
        this.username});

  factory RecoverPassword.fromJson(Map<String, dynamic> parsedJson) {
    return RecoverPassword(
        password: parsedJson['password'],
        verifiedPassword: parsedJson['verifiedPassword'],
        oldPassword: parsedJson['oldPassword'],
        role: parsedJson['rol'],
        username: parsedJson['username']);
  }

  Map<String, dynamic> toJson() => {
    'password': password,
    'verifiedPassword': verifiedPassword,
    'oldPassword': oldPassword,
    'rol': role,
    'username': username,
  };
}