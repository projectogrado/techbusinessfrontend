import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';

class DischargeDataSheet {
  int id;
  String deliverDate;
  AdmissionDataSheet admissionDataSheet;

  DischargeDataSheet(
      {this.id,
      this.deliverDate,
      this.admissionDataSheet});

  factory DischargeDataSheet.fromJson(Map<String, dynamic> parsedJson) {
    return DischargeDataSheet(
        id: parsedJson['id'],
        deliverDate: parsedJson['deliverDate'],
        admissionDataSheet:
            AdmissionDataSheet.fromJson(parsedJson['admissionDataSheet']));
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'deliverDate': deliverDate,
        'admissionDataSheet': admissionDataSheet.toJson()
      };

  Map<String, dynamic> toJsonRegistry() => {
        'deliverDate': deliverDate,
        'admissionDataSheet': admissionDataSheet.toJson()
      };
}
