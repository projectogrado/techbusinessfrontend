import 'package:techbusinessfrontend/src/model/process_model.dart';

class RepairRoute {
  int id;
  Process process;

  RepairRoute({this.id, this.process});

  factory RepairRoute.fromJson(Map<String, dynamic> parsedJson) {
    var processJson = parsedJson['process'];
    var getProcess = Process.fromJson(processJson);

    return RepairRoute(id: parsedJson['id'], process: getProcess);
  }

  Map<String, dynamic> toJson() => {'id': id, 'process': process.toJson()};
}
