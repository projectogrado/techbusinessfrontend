class User {
  int id;
  String username;
  String password;

  User({this.id, this.username, this.password});

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return User(
        id: parsedJson['id'],
        username: parsedJson['username'],
        password: parsedJson['password']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'username': username,
        'password': password,
      };

  Map<String, dynamic> toJsonRegistry() =>
      {'username': username, 'password': password};
}
