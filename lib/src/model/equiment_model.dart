

class Equipment {
  int id;
  String mark;
  String model;
  String type;
  String hardDisk;
  String ram;
  String board;
  String processor;
  String status;
  String readerCd;
  String other;

  Equipment(
      {this.id,
      this.mark,
      this.model,
      this.status,
      this.type,
      this.hardDisk,
      this.ram,
      this.board,
      this.processor,
      this.other,
      this.readerCd});

  factory Equipment.fromJson(Map<String, dynamic> parsedJson) {
    return Equipment(
        id: parsedJson['id'],
        model: parsedJson['model'],
        mark: parsedJson['mark'],
        status: parsedJson['status'],
        type: parsedJson['type'],
        hardDisk: parsedJson['hardDisk'],
        ram: parsedJson['ram'],
        processor: parsedJson['processor'],
        board: parsedJson['board'],
        other: parsedJson['other'],
        readerCd: parsedJson['readerCd']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'model': model,
        'mark': mark,
        'status': status,
        'type': type,
        'hardDisk': hardDisk,
        'ram': ram,
        'processor': processor,
        'board': board,
        'readerCd': readerCd,
        'other': other,
      };

  Map<String, dynamic> toJsonRegistry() => {
        'model': model,
        'mark': mark,
        'status': status,
        'type': type,
        'hardDisk': hardDisk,
        'ram': ram,
        'processor': processor,
        'board': board,
        'readerCd': readerCd,
        'other': other,
      };
}
