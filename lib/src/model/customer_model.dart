import 'admission_data_sheet_model.dart';

class Customer {
  int id;
  String name;
  String lastName;
  String email;
  String address;
  String phone;
  String typeDocument;
  String document;
  List<AdmissionDataSheet> admissionDataSheets;

  Customer({this.id,
    this.name,
    this.lastName,
    this.email,
    this.address,
    this.phone,
    this.document,
    this.typeDocument,
    this.admissionDataSheets});

  factory Customer.fromJson(Map<String, dynamic> parsedJson) {
    var admissions = parsedJson['admissionDataSheets'] as List;
    var admissionList = admissions != null
        ? admissions.map((e) => AdmissionDataSheet.fromJson(e))
        : List<AdmissionDataSheet>.empty();

    return Customer(
        id: parsedJson['id'],
        name: parsedJson['name'],
        lastName: parsedJson['lastname'],
        email: parsedJson['email'],
        address: parsedJson['address'],
        phone: parsedJson['phone'],
        document: parsedJson['document'],
        typeDocument: parsedJson['typeDocument'],
        admissionDataSheets: admissionList);
  }

  factory Customer.fromIgnoreList(Map<String, dynamic> parsedJson) {
    return Customer(
        id: parsedJson['id'],
        name: parsedJson['name'],
        lastName: parsedJson['lastname'],
        email: parsedJson['email'],
        address: parsedJson['address'],
        phone: parsedJson['phone'],
        document: parsedJson['document'],
        typeDocument: parsedJson['typeDocument']);
  }

  Map<String, dynamic> toJson() =>
      {
        'id': id,
        'name': name,
        'lastname': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'document': document,
        'typeDocument': typeDocument,
        'admissionDataSheets': admissionDataSheets != null
            ? admissionDataSheets.map((i) => i.toJson()).toList()
            : List.empty(),
      };

  Map<String, dynamic> toJsonRegistry() =>
      {
        'name': name,
        'lastname': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'document': document,
        'typeDocument': typeDocument,
        'admissionDataSheets': admissionDataSheets != null
            ? admissionDataSheets.map((i) => i.toJson()).toList()
            : List.empty(),
      };
}
