class ChecklistRepairKey {
  int repairRouteId;
  int workServiceId;

  ChecklistRepairKey({this.repairRouteId, this.workServiceId});

  factory ChecklistRepairKey.fromJson(Map<String, dynamic> parsedJson) {
    return ChecklistRepairKey(
        repairRouteId: parsedJson['repairRouteId'],
        workServiceId: parsedJson['workServiceId']);
  }

  Map<String, dynamic> toJson() =>
      {'repairRouteId': repairRouteId, 'workServiceId': workServiceId};
}
