import 'package:techbusinessfrontend/src/model/repair_route_model.dart';
import 'package:techbusinessfrontend/src/model/checklist_repair_key.dart';

class ChecklistRepair {
  ChecklistRepairKey id;
  bool status;
  String evidence;
  RepairRoute repairRoute;

  ChecklistRepair({this.id, this.status, this.evidence, this.repairRoute});

  factory ChecklistRepair.fromJson(Map<String, dynamic> parsedJson) {
    var repairRouteJson = parsedJson['repairRoute'];
    var getRepairRoute = RepairRoute.fromJson(repairRouteJson);

    var idJson = parsedJson['id'];
    var getId = ChecklistRepairKey.fromJson(idJson);

    return ChecklistRepair(
        id: getId,
        status: parsedJson['status'],
        evidence: parsedJson['evidence'],
        repairRoute: getRepairRoute);
  }

  Map<String, dynamic> toJson() => {
        'id': id.toJson(),
        'status': status,
        'evidence': evidence,
        // ignore: prefer_null_aware_operators
        'repairRoute': repairRoute != null ? repairRoute.toJson() : null
      };
}
