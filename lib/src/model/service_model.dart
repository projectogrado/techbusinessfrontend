import 'package:techbusinessfrontend/src/model/repair_route_model.dart';

class Service {
  int id;
  String name;
  String description;
  bool status;
  List<RepairRoute> repairRoute;

  Service({this.id, this.name, this.description, this.status, this.repairRoute});

  factory Service.fromJson(Map<String, dynamic> parsedJson) {
    return Service(
        id: parsedJson['id'],
        name: parsedJson['name'],
        description: parsedJson['description'],
        status: parsedJson['status'],
        repairRoute: List<RepairRoute>.from(parsedJson['repairRoute'].map((x)=> RepairRoute.fromJson(x)))
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'status': status,
        'repairRoute':repairRoute.map((e) => e.toJson()).toList()
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'description': description,
        'status': status,
        'repairRoute':repairRoute.map((e) => e.toJson()).toList()
  };
}
