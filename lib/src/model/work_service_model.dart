import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/checklist_repair_model.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';

class WorkService {
  int id;
  Service services;
  AdmissionDataSheet admissionDataSheet;
  List<ChecklistRepair> checklistRepairs;
  bool ready;

  WorkService(
      {this.checklistRepairs,
      this.id,
      this.services,
      this.admissionDataSheet,
      this.ready});

  factory WorkService.fromJson(Map<String, dynamic> parsedJson) {
    var checklistRepair = parsedJson['checklistRepairs'] as List;

    var checklistRepairs = checklistRepair != null
        ? checklistRepair.map((i) => ChecklistRepair.fromJson(i)).toList()
        : List.empty();

    var ready =
        checklistRepairs.where((element) => element.status == false).length ==
            checklistRepairs.length;

    return WorkService(
        id: parsedJson['id'],
        services: Service.fromJson(parsedJson['services']),
        checklistRepairs: checklistRepairs,
        ready: ready);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'services': services.toJson(),
        'checklistRepairs': checklistRepairs != null
            ? checklistRepairs.map((i) => i.toJson()).toList()
            : List.empty(),
        'admissionDataSheet': admissionDataSheet.toJsonRegistryList()
      };

  Map<String, dynamic> toJsonRegistry() => {
        'services': services.toJson(),
        'checklistRepairs': checklistRepairs != null
            ? checklistRepairs.map((i) => i.toJson()).toList()
            : List.empty(),
        'admissionDataSheet': admissionDataSheet.toJsonRegistryList()
      };
}
