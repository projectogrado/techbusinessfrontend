import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';

import 'type_user_model.dart';

class Employee {
  int id;
  String name;
  String lastName;
  String email;
  String address;
  String phone;
  String username;
  String password;
  bool status;
  String typeDocument;
  String document;
  List<Role> roles;
  List<AdmissionDataSheet> works;
  bool isTemporalPassword;
  String tokenNotification;

  Employee(
      {this.id,
      this.name,
      this.lastName,
      this.email,
      this.address,
      this.phone,
      this.username,
      this.password,
      this.status,
      this.document,
      this.typeDocument,
      this.roles,
      this.works,
      this.isTemporalPassword,
      this.tokenNotification});

  factory Employee.fromJson(Map<String, dynamic> parsedJson) {
    var role = parsedJson['roles'] as List;
    var work = parsedJson['works'] as List;

    var roleList = role != null
        ? role.map((i) => Role.fromJson(i)).toList()
        : List.empty();

    var workList = work != null
        ? work.map((i) => AdmissionDataSheet.fromJson(i)).toList()
        : List.empty();

    return Employee(
      id: parsedJson['id'],
      name: parsedJson['name'],
      lastName: parsedJson['lastname'],
      email: parsedJson['email'],
      address: parsedJson['address'],
      phone: parsedJson['phone'],
      username: parsedJson['username'],
      password: parsedJson['password'],
      status: parsedJson['status'],
      isTemporalPassword: parsedJson['isTemporalPassword'],
      document: parsedJson['document'],
      typeDocument: parsedJson['typeDocument'],
      tokenNotification: parsedJson['tokenNotification'],
      roles: roleList,
      works: workList,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'lastname': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'username': username,
        'password': password,
        'status': status,
        'isTemporalPassword': isTemporalPassword,
        'document': document,
        'typeDocument': typeDocument,
        'tokenNotification': tokenNotification,
        'roles': roles != null
            ? roles.map((i) => i.toJson()).toList()
            : List.empty(),
        'works':
            works != null ? works.map((i) => i.toJson()).toList() : List.empty()
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'lastname': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'username': username,
        'password': password,
        'status': status,
        'isTemporalPassword': isTemporalPassword,
        'document': document,
        'typeDocument': typeDocument,
        'tokenNotification': tokenNotification,
      };
}
