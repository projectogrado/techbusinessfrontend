class Process {
  int id;
  String name;
  String description;
  bool status;
  bool tem = false;

  Process({this.id, this.name, this.description, this.status});

  factory Process.fromJson(Map<String, dynamic> parsedJson) {
    return Process(
        id: parsedJson['id'],
        name: parsedJson['name'],
        description: parsedJson['description'],
        status: parsedJson['status']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'status': status,
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'description': description,
        'status': status,
      };
}
