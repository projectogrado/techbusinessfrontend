class Role {
  int id;
  String description;
  String name;

  Role({this.description, this.id, this.name});

  factory Role.fromJson(Map<String, dynamic> parsedJson) {
    return Role(
        id: parsedJson['id'],
        description: parsedJson['description'],
        name: parsedJson['name']);
  }

  Map<String, dynamic> toJson() =>
      {'id': id, 'description': description, 'name': name};
}
