import 'package:techbusinessfrontend/src/model/discharge_data_sheet_model.dart';

class PersonReceive {
  int id;
  String name;
  String lastName;
  String email;
  String address;
  String phone;
  String typeDocument;
  String document;
  String kinship;
  DischargeDataSheet dischargeDataSheet;

  PersonReceive(
      {this.id,
      this.name,
      this.lastName,
      this.email,
      this.address,
      this.phone,
      this.document,
      this.typeDocument,
      this.kinship,
      this.dischargeDataSheet});

  factory PersonReceive.fromJson(Map<String, dynamic> parsedJson) {
    return PersonReceive(
      id: parsedJson['id'],
      name: parsedJson['name'],
      lastName: parsedJson['lastname'],
      email: parsedJson['email'],
      address: parsedJson['address'],
      phone: parsedJson['phone'],
      document: parsedJson['document'],
      typeDocument: parsedJson['typeDocument'],
      kinship: parsedJson['kinship'],
      dischargeDataSheet: DischargeDataSheet.fromJson(parsedJson['dischargeDataSheet']),
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'lastname': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'document': document,
        'typeDocument': typeDocument,
        'kinship': kinship,
        'dischargeDataSheet': dischargeDataSheet.toJson(),
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'lastname': lastName,
        'email': email,
        'address': address,
        'phone': phone,
        'document': document,
        'typeDocument': typeDocument,
        'kinship': kinship,
        'dischargeDataSheet': dischargeDataSheet.toJson(),
      };
}
