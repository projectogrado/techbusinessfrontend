class LoginRequest {
  String password;
  String username;

  LoginRequest({this.password, this.username});

  factory LoginRequest.fromJson(Map<String, dynamic> parsedJson) {
    return LoginRequest(
        password: parsedJson['password'], username: parsedJson['username']);
  }

  Map<String, dynamic> toJson() => {
        'password': password,
        'username': username,
      };
}
