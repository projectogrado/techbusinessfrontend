import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';

import 'list_item_description_route.dart';

class CustomListItemRoute extends StatelessWidget {
  const CustomListItemRoute({Key key,
    this.title,
    this.description,
    this.status,
    this.icon,
    this.actionShow})
      : super(key: key);

  final String title;
  final String description;
  final String status;
  final Widget icon;
  final VoidCallback actionShow;



  Widget buttonAction() {
    return IconButton(
      icon: icon,
      onPressed: actionShow,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: ListItemDescriptionRoute(
              title: StringUtils.capitalize(title),
              description: StringUtils.capitalize(description),
              status: status,
            ),
          ),
          buttonAction(),
        ],
      ),
    );
  }
}
