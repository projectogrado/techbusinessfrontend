import 'package:flutter/material.dart';

class ListItemDescriptionRoute extends StatelessWidget {
  const ListItemDescriptionRoute({
    Key key,
    this.title,
    this.description,
    this.status,
  }) : super(key: key);

  final String title;
  final String description;
  final String status;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CheckboxListTile(
            controlAffinity: ListTileControlAffinity.leading,
            title: Text(title,
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18.0,
                )),
            value: status == 'true',
            onChanged: (bool value) {},
          ),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 25.0),
              child: Text(
                description,
                style: const TextStyle(fontSize: 16.0),
              )),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 25.0),
              child: Text(
                '$status',
                style: const TextStyle(fontSize: 12.0),
              ))
        ],
      ),
    );
  }
}
