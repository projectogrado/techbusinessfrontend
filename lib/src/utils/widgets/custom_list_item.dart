import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/widgets/list_item_description.dart';

class CustomListItem extends StatelessWidget {
  const CustomListItem(
      {Key key,
      this.title,
      this.description,
      this.status,
      this.actionDelete,
      this.actionEdit,
      this.actionShow,
      this.actionChecklist,
      this.actionContinue,
      this.actionClimb,
      this.actionStart})
      : super(key: key);

  final String title;
  final String description;
  final String status;
  final VoidCallback actionDelete;
  final VoidCallback actionEdit;
  final VoidCallback actionShow;
  final VoidCallback actionChecklist;
  final VoidCallback actionContinue;
  final VoidCallback actionStart;
  final Function actionClimb;

  List<String> dropDownOptions() {
    var options = <String>[];
    if (actionDelete != null) {
      options.add(Constants.actionDelete);
    }
    if (actionChecklist != null) {
      options.add(Constants.actionChecklist);
    }
    if (actionShow != null) {
      options.add(Constants.actionShow);
    }
    if (actionEdit != null) {
      options.add(Constants.actionEdit);
    }
    if (actionContinue != null) {
      options.add(Constants.actionContinue);
    }
    if (actionStart != null) {
      options.add(Constants.actionStart);
    }
    if(actionClimb != null){
      options.add(Constants.actionClimb);
    }

    return options;
  }

  Widget dropdown() {
    return DropdownButtonHideUnderline(
      child: DropdownButton<String>(
        isExpanded: false,
        icon: const Icon(Icons.more_vert),
        iconSize: 16,
        onChanged: (String optionSelected) {
          _actions(optionSelected);
        },
        items: dropDownOptions().map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(StringUtils.capitalize(value)),
          );
        }).toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: ListItemDescription(
              title: StringUtils.capitalize(title),
              description: StringUtils.capitalize(description),
              status: status,
            ),
          ),
          dropdown(),
        ],
      ),
    ));
  }

  void _actions(String actionSelected) {
    switch (actionSelected) {
      case Constants.actionDelete:
        {
          actionDelete();
        }
        break;

      case Constants.actionEdit:
        {
          actionEdit();
        }
        break;

      case Constants.actionChecklist:
        {
          actionChecklist();
        }
        break;

      case Constants.actionShow:
        {
          actionShow();
        }
        break;
      case Constants.actionStart:
        {
          actionStart();
        }
        break;
      case Constants.actionContinue:
        {
          actionContinue();
        }
        break;
      case Constants.actionClimb:
        {
          actionClimb();
        }
    }
  }
}
