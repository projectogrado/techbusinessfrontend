import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';

class Expansionpanel extends StatefulWidget {
  final WorkService workService;

  const Expansionpanel({Key key, this.workService}) : super(key: key);

  @override
  ExpansionPanelState createState() => ExpansionPanelState();
}

class NewItem {
  bool isExpanded;
  final String header;
  final Widget body;
  final Icon iconpic;

  NewItem(this.isExpanded, this.header, this.body, this.iconpic);
}

class ExpansionPanelState extends State<Expansionpanel>
    with SingleTickerProviderStateMixin {
  ExpansionPanelState();

  ListView listCriteria;
  WorkService workService;
  List<NewItem> items;
  List<Step> steps;
  int _currentStep = 0;
  int _size = 0;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    workService = widget.workService;
    steps = generateStep();
    items = listItem();
  }

  List<Step> generateStep() {
    var steppers = <Step>[];
    _size = workService.checklistRepairs.length;
    workService.checklistRepairs.forEach((element) {
      steppers.add(Step(
          title: Text(
            element.repairRoute.process.name,
            style: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w400,
            ),
          ),
          content: Column(
            children: [
              Text(
                element.repairRoute.process.description,
                style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w400,
                ),
              ),
              IconButton(icon: Icon(Icons.add_a_photo_outlined)),
              Checkbox(value: element.repairRoute.process.status),
            ],
          ),
          isActive: true,
          state: StepState.indexed));
    });
    return steppers;
  }

  List<NewItem> listItem() {
    return <NewItem>[
      NewItem(
          false, // isExpanded ?
          workService.services.name, // header
          Container(
              child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      Stepper(
                        type: StepperType.vertical,
                        physics: ScrollPhysics(),
                        currentStep: _currentStep,
                        controlsBuilder: (BuildContext context,
                            {VoidCallback onStepContinue,
                            VoidCallback onStepCancel}) {
                          return Row(children: [
                            onStepContinue != null
                                ? TextButton(
                                    onPressed: onStepContinue,
                                    child: Text('Siguiente'))
                                : Text(''),
                            onStepCancel != null
                                ? TextButton(
                                    onPressed: onStepCancel,
                                    child: Text('Volver'))
                                : Text('')
                          ]);
                        },
                        onStepTapped: tapped,
                        onStepContinue: _currentStep < _size ? continued : null,
                        onStepCancel: _currentStep < _size ? cancel : null,
                        steps: steps,
                      ),
                    ],
                  ))),
          Icon(
            Icons.electrical_services_sharp,
            size: 24,
          ))
    ];
  }

  @override
  Widget build(BuildContext context) {
    listCriteria = ListView(
      children: [
        Padding(
          padding: EdgeInsets.all(10.0),
          child: ExpansionPanelList(
            expansionCallback: (int index, bool isExpanded) {
              setState(() {
                items[index].isExpanded = !items[index].isExpanded;
              });
            },
            children: items.map((NewItem item) {
              return ExpansionPanel(
                headerBuilder: (BuildContext context, bool isExpanded) {
                  return ListTile(
                      leading: item.iconpic,
                      title: Text(
                        item.header,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ));
                },
                isExpanded: item.isExpanded,
                body: item.body,
              );
            }).toList(),
          ),
        ),
      ],
    );
    var scaffold = Scaffold(
      body:listCriteria
    );
    return scaffold;
  }

  void tapped(int step) {
    // ignore: always_declare_return_types
    return setState(() => _currentStep = step);
  }

  void continued() {
    // ignore: always_declare_return_types
    return _currentStep < _size ? setState(() => _currentStep += 1) : null;
  }

  void cancel() {
    // ignore: always_declare_return_types
    return _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }
}
