import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

void confirmDialog(BuildContext context, String title, String message) {}

void showAlert(BuildContext context, String message) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Center(child: Text('error')),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.error,
                color: ConstantsColors.error,
              ),
              Expanded(
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: ConstantsColors.black,
                  ),
                ),
              )
            ],
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(Constants.btnCancel),
            )
          ],
        );
      });
}

void showWarning(BuildContext context,String title, String message) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Center(child: Text(title)),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.warning_amber_outlined,
                color: ConstantsColors.orange,
              ),
              Expanded(
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: ConstantsColors.black,
                  ),
                ),
              )
            ],
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(Constants.btnCancel),
            )
          ],
        );
      });
}

void showCustomModalBottomSheet(
    BuildContext context, List<Widget> children, double height) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Container(
            padding: const EdgeInsets.all(5.0),
            height: height,
            child: Column(children: children));
      });
}

void showDialogSuccess(BuildContext context, String message) {
  showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          title: Center(child: Text('Success')),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.check,
                color: ConstantsColors.success,
              ),
              Expanded(
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: ConstantsColors.black,
                  ),
                ),
              )
            ],
          ),
        );
      });
}
