import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/ui/administrator/administrator_home_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/customer/customer_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/employee/employee_create_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/employee/employee_search_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/employee/employee_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/equipment/equipment_detail_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/equipment/equipment_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/process/process_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/service/detail_service_ui.dart';
import 'package:techbusinessfrontend/src/ui/administrator/service/service_ui.dart';
import 'package:techbusinessfrontend/src/ui/authentication/auth_login_ui.dart';
import 'package:techbusinessfrontend/src/ui/authentication/auth_restore_password_ui.dart';
import 'package:techbusinessfrontend/src/ui/employee/admission/admission_data_sheet_ui.dart';
import 'package:techbusinessfrontend/src/ui/employee/discharge/delivery_form_ui.dart';
import 'package:techbusinessfrontend/src/ui/employee/discharge/dicharge_data_sheet_ui.dart';
import 'package:techbusinessfrontend/src/ui/employee/employee_home_ui.dart';
import 'package:techbusinessfrontend/src/ui/employee/routeWork/routeWork.dart';
import 'package:techbusinessfrontend/src/ui/employee/routeWork/route_work_detail.dart';
import 'package:techbusinessfrontend/src/ui/employee/widget/reader_qr_ui.dart';
import 'package:techbusinessfrontend/src/ui/shared/admission/admission_detail_ui.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class RouterCustom {
  static Route<dynamic> onGenerateRoute(settings) {
    switch (settings.name) {
      case Constants.authUiRoute:
        return MaterialPageRoute(builder: (_) => AuthUi());
        break;
      case Constants.administratorHomeRoute:
        return MaterialPageRoute(builder: (_) => AdministratorUi());
        break;
      case Constants.serviceUiRoute:
        return MaterialPageRoute(builder: (_) => ServiceUI());
        break;
      case Constants.processUiRoute:
        return MaterialPageRoute(builder: (_) => ProcessUI());
        break;
      case Constants.employeeCreateUiRoute:
        return MaterialPageRoute(builder: (_) => EmployeeUi());
        break;
      case Constants.employeeSearchUiRoute:
        return MaterialPageRoute(builder: (_) => EmployeeSeachUi());
        break;
      case Constants.employeeUiRoute:
        return MaterialPageRoute(builder: (_) => EmployeeListUi());
        break;
      case Constants.equipmentUiRoute:
        return MaterialPageRoute(builder: (_) => EquipmentUi());
        break;
      case Constants.customerUiRoute:
        return MaterialPageRoute(builder: (_) => CustomerUi());
        break;
      case Constants.employeeHomeUiRoute:
        return MaterialPageRoute(builder: (_) => EmployeeHomeUi());
        break;
      case Constants.employeeDischargeUiRoute:
        var args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => DischargeDataSheetUi(
                  dischargeDataSheet: args[Constants.labelDischargeDataSheet],
                ));
        break;
      case Constants.employeeAdmissionUiRoute:
        return MaterialPageRoute(builder: (_) => AdmissionDataSheetUi());
        break;
      case Constants.employeeReaderQrUiRoute:
        return MaterialPageRoute(builder: (_) => ReaderQrUi());
        break;

      case Constants.customerUiRoute:
        return MaterialPageRoute(builder: (_) => CustomerUi());
        break;
      case Constants.resetPasswordUiRoute:
        var args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => RestorePasswordUi(
                  username: args[Constants.labelUsername],
                  role: args[Constants.labelRol],
                ));
        break;

      case Constants.serviceDetailUiRoute:
        var args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) =>
                DetailServiceUI(service: args[Constants.labelService]));
        break;

      case Constants.equipmentDetailUiRoute:
        var args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) =>
                DetailEquipmentUI(equipment: args[Constants.labelEquipment]));
        break;

      case Constants.deliveryPersonRoute:
        var args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => PersonReceiveUi(
                dischargeDataSheet: args[Constants.labelDischargeDataSheet]));
        break;

      case Constants.routeWorkRoute:
        var args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) =>
                RouteWorkUi(admissionId: args[Constants.labelWorkService]));
        break;

      case Constants.routeWorkDetailRoute:
        var args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => RouteWorkDetailUi(
                  workService: args[Constants.labelService],
                  id: args[Constants.labelId]
                ));
        break;

      case Constants.admissionUiRoute:
        var args = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => DetailAdmissionUI(id: args[Constants.labelId]));
        break;

      default:
        return MaterialPageRoute(builder: (_) => AdministratorUi());
    }
  }
}
