import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class TokenHelper {
  final _storage = FlutterSecureStorage();
  String accessToken;

  Future<Map<String, String>> getAllValue() async {
    return _storage.readAll();
  }

  Future<String> getValueByKey(String key) async {
    return _storage.read(key: key);
  }

  Future deleteValue(String key) async {
    await _storage.delete(key: key);
  }

  Future deleteAllValue() async {
    await _storage.deleteAll();
  }

  Future writeValue(String key, String value) async {
    await _storage.write(key: key, value: value);
  }

  Future getToken() async {
    accessToken = await getValueByKey('token');
  }

  void tokenInitializated(String token) async {
    await _storage.write(key: 'token', value: token);
    accessToken = await _storage.read(key: 'token');
  }

  bool existToken() {
    var token;
    _storage.read(key: 'token')
        .then((value) =>  token = value);
    return token != null;
  }

  void logout() {
    _storage.delete(key: 'token');
  }

  Future get accessTokenL => getToken();
}
