import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

class Constants {
  static const String urlAuthority =
      '127.0.0.1:8089';
  static const String pathBase = '/api/tech_business';
  static const String contentTypeHeader = 'application/json; charset=utf-8';
  static const String authorizationHeader = 'Bearer ';

  //Url Backend authentication
  static const String urlLogin = pathBase + '/auth/login';
  static const String urlRegisterUser = pathBase + '/auth/register';
  static const String urlResetPassword = pathBase + '/password-reset';

  //Url Backend employee
  static const String urlEmployee = pathBase + '/employee';

  //Url Backend personReceive
  static const String urlPersonReceive = pathBase + '/person-receive';

  //Url Backend customer
  static const String urlCustomer = pathBase + '/customer';

  //Url Backend process
  static const String urlProcess = pathBase + '/process';

  //Url Backend services
  static const String urlService = pathBase + '/services';

  static const String urlEquipment = pathBase + '/equipment';

  //Url Backend dischargedatasheet
  static const String urlDischarge = pathBase + '/discharge';

  //Url Backend admission data sheet
  static const String urlAdmission = pathBase + '/admission';

  //Url Backend work service
  static const String urlWorkService = pathBase + '/work-service';

  //App
  static const String appName = 'Tech Business';
  static const String logout = 'Cerrar Sesión';

  //appBarTitle
  static const String appBarMenu = 'Tech Business menu';
  static const String appBarMenuEmployee = 'Tech Business menu Empleado';
  static const String appBarProcessList = 'Lista de procesos';
  static const String appBarRegisterEmployee = 'Registrar Trabajador';
  static const String appBarRegisterPersonReceive = 'Registar persona que recibe equipo';
  static const String appBarDetailEquipment = 'Detalles del equipo';
  static const String appBarDichargeDataSheet = 'Ficha de egreso';
  static const String appBarDetailAdmission = 'Ficha tecnica de ingreso';
  static const String appBarWorkRoute = 'Ruta de reparación';

  //MLMenuOrden
  static const String mlItemContentOrder = 'Ordenar por:';
  static const String mlSubContentIdOrder = 'ID';
  static const String mlSubContentMarkOrder = 'Mark';

  //MLMenuFilter
  static const String mlItemContentFilter = 'Filtar por:';
  static const String mlSubContentMarkFilter = 'Marca';
  static const String mlSubContentIdFilter = 'ID';

  //MLMenuGetAll
  static const String mlItemContentGetAll = 'Traer lista';

  //Route
  static const String administratorHomeRoute = '/';
  static const String authUiRoute = '/auth_login_ui';
  static const String processUiRoute = '/process_ui';
  static const String serviceUiRoute = '/service_ui';
  static const String serviceDetailUiRoute = '/detail_service_ui';
  static const String equipmentDetailUiRoute = '/detail_equipment_ui';
  static const String equipmentUiRoute = '/equipment_ui';
  static const String customerUiRoute = '/customer_ui';
  static const String configUiRoute = '/config_ui';
  static const String employeeCreateUiRoute = '/employee_create_ui';
  static const String employeeSearchUiRoute = '/employee_search_ui';
  static const String employeeUiRoute = '/employee_ui';
  static const String admissionUiRoute = '/admission_detail_ui';
  static const String employeeHomeUiRoute = '/employee_home_ui';
  static const String employeeReaderQrUiRoute = '/reader_qr_ui';
  static const String employeeDischargeUiRoute = '/discharge_ui';
  static const String employeeAdmissionUiRoute = '/employee_admission_ui';
  static const String resetPasswordUiRoute = '/reset_password_ui';
  static const String deliveryPersonRoute = '/delivery_person_ui';
  static const String routeWorkRoute = '/route_work_ui';
  static const String routeWorkDetailRoute = '/route_work_detail_ui';

  //MenuTitle
  static const String menuTitleEmployee = 'Trabajadores';
  static const String menuTitleProcess = 'Procesos';
  static const String menuTitleService = 'Servicios';
  static const String menuTitleCustomer = 'Clientes';
  static const String menuTitleConfig = 'Configuración';
  static const String menuTitleEquipment = 'Equipos';
  static const String menuTitleDischargedataSheet = 'Fecha de egreso';

  //Message
  static const String messageSuccessfulRegister = '¡Registro exitoso!';
  static const String messageLogoutConfirm =
      'Esta seguro de finalizar la sesión.';
  static const String messageAlertDialog2 =
      'Esta seguro de finalizar la acción.';
  static const String messageAlertDialog3 = 'Formule el proceso.';
  static const String loginSuccess = 'inicio de sesion exitoso';
  static const String insertSuccess = 'registro exitoso';
  static const String updateSuccess = 'actualización exitosa';
  static const String deleteSuccess = 'Se eliminó correctamente';
  static const String loginTitle = 'Login';
  static const String registerTitle = 'Register';
  static const String tittleDialog = 'Información';

  //QR
  static const String scanToolTip = 'Scan';
  static const String errorQr = 'El scan fallo o codigo incorrecto';
  static const String permissiondeDenied = 'Permiso denegado';
  static const String flashOn = 'Flash on';
  static const String flashOff = 'Flash off';
  static const String flashCancel = 'cancel';

  //Button
  static const String btnAccept = 'Aceptar';
  static const String btnCancel = 'Cancelar';
  static const String btnRegister = 'Registrar';
  static const String btnCreates = 'Crear';
  static const String btnSend = 'Enviar';
  static const String btnUpdates = 'Actualizar';
  static const String btnExit = 'Cerrar';
  static const String btnSignIn = 'Ingresar';

  //customer Card
  static const String cardName = 'Nombre:  ';
  static const String cardLastName = 'Apellido:  ';
  static const String cardEmail = 'Email:  ';
  static const String cardCellPhone = 'Celular:  ';
  static const String cardAddresses = 'Dirección:  ';
  static const String cardDocument = 'Documento:  ';

  //Label forms
  static const String labelName = 'Nombre ';
  static const String labelUsername = 'Username ';
  static const String labelMessage = 'Message';
  static const String labelRol = 'Rol';
  static const String labelLastName = 'Apellido ';
  static const String labelEmail = 'Correo Electronico ';
  static const String labelCCorNit = 'Cedula o Nit ';
  static const String labelCellPhone = 'Celular ';
  static const String labelAddresses = 'Dirección ';
  static const String labelDescription = 'Descripción';
  static const String labelProcessState = 'Estado del proceso';
  static const String labelSearchDocument = 'Documento';

  //label action
  static const String labelCreates = 'Crear';
  static const String labelAdd = 'Agregar';
  static const String labelUpdates = 'Actualizar';

  //label module
  static const String labelEmployee = 'Trabajadores';
  static const String labelProcess = 'Procesos';
  static const String labelChecklistItem = 'Item check';
  static const String labelService = 'Servicios';
  static const String labelWorkService = 'workService';
  static const String labelId = 'Id';
  static const String labelCustomer = 'Clientes';
  static const String labelConfig = 'Configuración';
  static const String labelEquipment = 'Equipos';
  static const String labelDischargeDataSheet = 'Ficha egresos';

  //hint
  static const String someText = 'Por favor ingrese un texto';
  static const String hintSearchDocument = 'Por favor ingrese un documento';

  //validation text fields
  static const String structure = 'La estructura no coincide con la solicitada';

  static const String validateName = 'El nombre es necesario';
  static const String validateLastName = 'El apellido es necesario';
  static const String fieldStructureUpperAndLower =
      'El campo solo puede contener a-z y A-Z';
  static const String patternUpperAndLower = r'(^[a-zA-Z ]*$)';

  static const String validateEmail = 'El correo es necesario';
  static const String emailStructure = 'Correo invalido';
  static const String patternEmail =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  static const String validateMobile = 'El telefono es necesario';
  static const String mobileStructure = 'Debe tener 10 digitos';
  static const String patterMobile = r'(^[3][0-9]*$)';

  static const String identificationStructure = 'Debe tener 10 digitos';
  static const String validateIdentification =
      'La documentacion o Nit es necesario';
  static const String patterIdentification = r'(^[3][0-9]*$)';

  static const String confirmDelete = 'Are you sure you want to delete?';

  static const String actionDelete = 'eliminar';
  static const String actionSave = 'guardar';
  static const String actionEdit = 'editar';
  static const String actionContinue = 'continuar';
  static const String actionStart = 'iniciar';
  static const String actionClimb = 'Escalar';
  static const String actionShow = 'ver más';
  static const String actionDownload = 'descargar';
  static const String actionSendMail = 'enviar';
  static const String actionAddService = 'add';
  static const String actionChecklist = 'lista de chequeo';

  static const List<String> actionsChecklistMenu = ['guardar', 'eliminar'];
  static const List<String> actionsAdmission = ['descargar', 'enviar','add'];
  static const List<String> actionsEquipmentMenu = ['filtrar', 'ordenar'];

  static const List<Map<String, dynamic>> itemSelectTypeDocument = [
    {
      'value': 'CC',
      'label': 'Cedula Ciudadania',
      'textStyle': TextStyle(color: ConstantsColors.primaryColor),
    },
    {
      'value': 'TI',
      'label': 'Trajeta Identidad',
      'textStyle': TextStyle(color: ConstantsColors.primaryColor),
    },
    {
      'value': 'CE',
      'label': 'Cedula extranjeria',
      'textStyle': TextStyle(color: ConstantsColors.primaryColor),
    },
  ];
}
