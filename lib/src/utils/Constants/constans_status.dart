class ConstantsStatus {

  static const String byAssigning = 'BY_ASSIGNING';
  static const String toDo = 'TO_DO';
  static const String inRepair = 'IN_REPAIR';
  static const String repaired = 'REPAIRED';
  static const String notRepaired = 'NOT_REPAIRED';
  static const String novelty = 'NOVELTY';
  static const String delivered = 'DELIVERED';

}