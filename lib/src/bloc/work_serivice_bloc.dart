import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/checklist_repair_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class WorkServiceBloc {
  final _repository = GeneralRepository();
  var _apiResponse = ApiResponse();
  List<WorkService> _list = [];
  final _workServiceListController =
  StreamController<List<WorkService>>.broadcast();

  Stream<List<WorkService>> get workServiceStream =>
      _workServiceListController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  WorkServiceBloc();

  Future<ApiResponse> create(WorkService workService) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.createWorkService(workService);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> addCheckLit(List<ChecklistRepair> list, int id) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.addCheckListWorkService(list, id);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  // ignore: missing_return
  Future<ApiResponse> loadWorkServiceById(int id) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getAllWorkByAdmission(id);
    if(_apiResponse.statusResponse == HttpStatus.ok){
      _list = _apiResponse.payload;
      _list.forEach((element) { element.ready = element.checklistRepairs.where((checked) => checked.status == false).isEmpty; });
      _workServiceListController.add(_list);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return apiResponse;
  }

  void dispose() {
    _workServiceListController.close();
  }
}
