import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class AdmissionDataSheetBloc {
  final _repository = GeneralRepository();
  var _admission = AdmissionDataSheet();
  final _admissionController = StreamController<AdmissionDataSheet>.broadcast();

  ApiResponse _apiResponse = ApiResponse();

  ApiResponse get apiResponse => _apiResponse;

  Stream<AdmissionDataSheet> get admission =>
      _admissionController.stream.asBroadcastStream();

  AdmissionDataSheetBloc();

  Future<ApiResponse> create(
      AdmissionDataSheet admissionDataSheet, bool editedCustomer) async {
    await _repository.accessTokenL;
    return _registerAdmission(admissionDataSheet);
  }

  Future<ApiResponse> _registerAdmission(
      AdmissionDataSheet admissionDataSheet) async {
    _apiResponse = await _repository.createAdmission(admissionDataSheet);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future loadDataAdmission(int id) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getAdmissionById(id);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _admission = _apiResponse.payload;
      _admissionController.add(_admission);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future downloadPdf(int id) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getPdfById(id);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future<ApiResponse> climbAdmission(int id) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.climbAdmission(id);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> sendPdfForEmail(int id) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.sendForMail(id);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      return _apiResponse;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future<ApiResponse> updateStatusAdmission(int id, String status) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.updateStatusAdmission(id, status);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  void dispose() {
    _admissionController.close();
  }
}
