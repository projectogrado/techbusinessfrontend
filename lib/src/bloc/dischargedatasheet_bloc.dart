import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/discharge_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';

class DischargeDataSheetBloc {
  final _repository = GeneralRepository();
  ApiResponse _apiResponse = ApiResponse();
  final _dischargeDataSheetListController =
      StreamController<List<DischargeDataSheet>>.broadcast();

  // ignore: close_sinks
  final _dischargeDataSheetController =
      StreamController<DischargeDataSheet>.broadcast();

  DischargeDataSheet _dischargeDataSheet;

  Stream<DischargeDataSheet> get dischargeDataSheet =>
      _dischargeDataSheetController.stream.asBroadcastStream();

  Stream<List<DischargeDataSheet>> get dischargeDataSheetList =>
      _dischargeDataSheetListController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  DischargeDataSheetBloc();

  Future<ApiResponse> initializeData(String data) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getDischargeDataSheetById(data);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _dischargeDataSheet = _apiResponse.payload;
      _dischargeDataSheetController.add(_dischargeDataSheet);
      return _apiResponse;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  void dispose() {
    _dischargeDataSheetController.close();
    _dischargeDataSheetListController.close();
  }
}
