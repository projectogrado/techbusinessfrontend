import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/customer_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class CustomerBloc {
  final _repository = GeneralRepository();
  var _apiResponse = ApiResponse();
  List<Customer> _initialList;
  final _customerListController = StreamController<List<Customer>>.broadcast();

  ApiResponse get apiResponse => _apiResponse;

  Stream<List<Customer>> get customerList =>
      _customerListController.stream.asBroadcastStream();

  CustomerBloc();

  Future<ApiResponse> initializeData() async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getAllCustomer();
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _initialList = _apiResponse.payload;
      _customerListController.add(_initialList);
      return _apiResponse;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> create(Customer customer) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.registerCustomer(customer);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Customer customer) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.updateCustomer(customer);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.updateSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> getCustomerByDocument(String document) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getCustomerByDocument(document);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }
}
