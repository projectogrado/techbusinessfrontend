import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/login_request_model.dart';
import 'package:techbusinessfrontend/src/model/recover_password.dart';
import 'package:techbusinessfrontend/src/model/success_login_model.dart';
import 'package:techbusinessfrontend/src/model/user_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/token_helper.dart';

class AuthBloc {
  final _repository = GeneralRepository();
  final _tokenHelper = TokenHelper();
  ApiResponse _apiResponse = ApiResponse();
  final _loginRequestController = StreamController<LoginRequest>.broadcast();
  final _userVeterinaryController = StreamController<User>.broadcast();

  Stream<LoginRequest> get login => _loginRequestController.stream.asBroadcastStream();

  Stream<User> get sigUp => _userVeterinaryController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  AuthBloc();

  Future<ApiResponse> signIn(LoginRequest loginRequest) async {
    _apiResponse = await _repository.login(loginRequest);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      SuccessLogin successLogin = _apiResponse.payload;
      _tokenHelper.tokenInitializated(successLogin.token);
      _apiResponse.message = Constants.loginSuccess;
    } else if (_apiResponse.statusResponse == HttpStatus.permanentRedirect) {
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> signUp(User user) async {
    _apiResponse = await _repository.signUp(user);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> recoverPassword(String username) async {
    _apiResponse = await _repository.resetPassword(username);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> updatePassword(RecoverPassword recoverPassword) async {
    _apiResponse = await _repository.updatePassword(recoverPassword);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  void dispose() {
    _loginRequestController.close();
    _userVeterinaryController.close();
  }
}
