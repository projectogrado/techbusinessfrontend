import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/repair_route_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class ServiceBloc {
  final _repository = GeneralRepository();
  ApiResponse _apiResponse = ApiResponse();
  final _serviceListController = StreamController<List<Service>>.broadcast();
  final _optionsDropDown = StreamController<List<Service>>.broadcast();
  final _checklistController = StreamController<List<RepairRoute>>.broadcast();
  final _serviceController = StreamController.broadcast();
  List<Service> _initialList;

  Stream<Service> get service => _serviceController.stream.asBroadcastStream();

  Stream<List<RepairRoute>> get processList =>
      _checklistController.stream.asBroadcastStream();

  Stream<List<Service>> get serviceList =>
      _serviceListController.stream.asBroadcastStream();

  Stream<List<Service>> get optionsList =>
      _optionsDropDown.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  ServiceBloc();

  Future initializeData() async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getAllService();
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _initialList = _apiResponse.payload;
      _serviceListController.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future initializeDataDropDown(AdmissionDataSheet admissionDataSheet) async {
    await _repository.accessTokenL;
    var listId = admissionDataSheet.workServices.map((e) => e.services.id).toList();
    _apiResponse = await _repository.getAllService();
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _initialList = _apiResponse.payload;
      _initialList.removeWhere((element) =>  listId.contains(element.id));
      _optionsDropDown.add(_initialList);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
  }

  Future initializeDataDetail(Service service) async {
    await _repository.accessTokenL;
    _checklistController.add(service.repairRoute);
  }

  Future<List<Process>> getProcess() async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.getAllProcess();
    return _apiResponse.payload;
  }

  Future<ApiResponse> create(Service service) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.createService(service);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Service service) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.updateService(service);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.updateSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> delete(Service service) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.deleteService(service.id);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = _apiResponse.payload;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }


  void dispose() {
    _serviceController.close();
    _serviceListController.close();
    _checklistController.close();
  }
}
