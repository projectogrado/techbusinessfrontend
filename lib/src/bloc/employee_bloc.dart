import 'dart:async';
import 'dart:io';

import 'package:f_logs/model/flog/flog.dart';
import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/repository/general_repository.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';

class EmployeeBloc {
  final _repository = GeneralRepository();
  var _apiResponse = ApiResponse();

  final _employeeController = StreamController<Employee>.broadcast();
  final _employeeListController = StreamController<List<Employee>>.broadcast();
  final _worksListController =
      StreamController<List<AdmissionDataSheet>>.broadcast();

  List<AdmissionDataSheet> _work;

  Employee currentEmployee;

  List<Employee> _initialList;

  Stream<Employee> get employeeActual =>
      _employeeController.stream.asBroadcastStream();

  Stream<List<Employee>> get employeeList =>
      _employeeListController.stream.asBroadcastStream();

  Stream<List<AdmissionDataSheet>> get workList =>
      _worksListController.stream.asBroadcastStream();

  ApiResponse get apiResponse => _apiResponse;

  EmployeeBloc();

  Future<ApiResponse> create(Employee employee) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.register(employee);
    if (_apiResponse.statusResponse == HttpStatus.created) {
      _apiResponse.message = Constants.insertSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> update(Employee employee) async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.updateEmployee(employee);
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _apiResponse.message = Constants.updateSuccess;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  Future<ApiResponse> getWorks() async {
    await _repository.accessTokenL;

    _apiResponse = await _repository.getEmployeeLogged();
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      currentEmployee = _apiResponse.payload;
      _work = currentEmployee.works;
      _worksListController.add(_work);
      _employeeController.add(currentEmployee);
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }

    return _apiResponse;
  }

  Future<ApiResponse> initializeData() async {
    await _repository.accessTokenL;
    _apiResponse = await _repository.listEmployee();
    if (_apiResponse.statusResponse == HttpStatus.ok) {
      _initialList = _apiResponse.payload;
      _apiResponse.message = Constants.insertSuccess;
      _employeeListController.add(_initialList);
      return _apiResponse;
    } else {
      ErrorApiResponse error = _apiResponse.payload;
      FLog.error(text: error.message);
    }
    return _apiResponse;
  }

  void dispose() {
    _employeeController.close();
    _employeeListController.close();
    _worksListController.close();
  }
}
