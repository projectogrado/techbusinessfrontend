import 'dart:io';

import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:techbusinessfrontend/src/bloc/admission_data_sheet_bloc.dart';
import 'package:techbusinessfrontend/src/bloc/service_bloc.dart';
import 'package:techbusinessfrontend/src/bloc/work_serivice_bloc.dart';
import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/customer_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';
import 'package:techbusinessfrontend/src/ui/shared/silverDelegate.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/custom_list_item.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';

class DetailAdmissionUI extends StatefulWidget {
  final int id;

  const DetailAdmissionUI({Key key, this.id}) : super(key: key);

  @override
  DetailAdmissionUiState createState() => DetailAdmissionUiState();
}

class DetailAdmissionUiState extends State<DetailAdmissionUI>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ServiceBloc _serviceBloc;
  WorkServiceBloc _workServiceBloc;
  int id;
  AdmissionDataSheetBloc _admissionDataSheetBloc;

  @override
  void dispose() {
    super.dispose();
    _admissionDataSheetBloc.dispose();
    _serviceBloc.dispose();
    _workServiceBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    id = widget.id;
    _admissionDataSheetBloc = AdmissionDataSheetBloc();
    _serviceBloc = ServiceBloc();
    _workServiceBloc = WorkServiceBloc();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _admissionDataSheetBloc.loadDataAdmission(id);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(Constants.appBarDetailAdmission,
              style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  color: ConstantsColors.blackLight)),
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_rounded,
                color: ConstantsColors.moderateGray),
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: [dropdown()],
        ),
        body: _buildStream());
  }

  Widget dropdown() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            isExpanded: false,
            icon: const Icon(Icons.cloud_download_outlined),
            onChanged: (String optionSelected) {
              _actions(optionSelected, id);
            },
            items: Constants.actionsAdmission
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(StringUtils.capitalize(value)),
              );
            }).toList(),
          ),
        ));
  }

  void _actions(String actionSelected, id) {
    switch (actionSelected) {
      case Constants.actionDownload:
        {
          _admissionDataSheetBloc.downloadPdf(id).then(
              (value) => {showDialogSuccess(context, 'Descarga exitosa')});
        }
        break;

      case Constants.actionSendMail:
        {
          _admissionDataSheetBloc
              .sendPdfForEmail(id)
              .then((value) => {showDialogSuccess(context, 'Envio exitoso')});
        }
        break;
      case Constants.actionAddService:
        {
          _serviceBloc
              .initializeDataDropDown(
                  _admissionDataSheetBloc.apiResponse.payload)
              .then((value) {
            showDialog(
              context: context,
              builder: (_) => _buildFormDialog(context),
            );
          });
        }
        break;
    }
  }

  Widget _buildStream() {
    return StreamBuilder<AdmissionDataSheet>(
        stream: _admissionDataSheetBloc.admission,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return _buildSilver(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  SliverList _infoCustomer(Customer customer) {
    return SliverList(
        delegate: SliverChildListDelegate([
      Card(
        color: ConstantsColors.purple,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Column(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.all(16.0),
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                        child: Text(
                      'Datos Cliente',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: ConstantsColors.white),
                    )))),
            Padding(
                padding: const EdgeInsets.all(16.0),
                child: _buildInfoCustomer(customer)),
          ],
        ),
      ),
    ]));
  }

  SliverList _infoEquipment(Equipment equipment) {
    return SliverList(
        delegate: SliverChildListDelegate([
      Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        elevation: 8.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Container(
            color: ConstantsColors.purple,
            width: 10,
            height: 587,
          ),
          Expanded(
              child: Column(
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                          child: Text(
                        'Información del equipo',
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: ConstantsColors.blackLight),
                      )))),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: _buildInfoEquipment(equipment)),
            ],
          ))
        ]),
      ),
    ]));
  }

  Widget _infoService(List<WorkService> data) {
    return SliverList(
        delegate: SliverChildBuilderDelegate((context, index) {
      return Column(children: [
        Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Container(
                    child: Text(
                  'Servicios',
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      color: ConstantsColors.blackLight),
                )))),
        Padding(
            padding: const EdgeInsets.all(16.0),
            child: CustomListItem(
              title: data[index].id.toString(),
              description: data[index].services.name,
              status: data[index].services.description,
            )),
        Divider()
      ]);
    }, childCount: data.length));
  }


  SliverPersistentHeader _infoAdmission(AdmissionDataSheet admissionDataSheet) {
    return SliverPersistentHeader(
      pinned: false,
      delegate: SliverAppBarDelegate(
        minHeight: 60.0,
        maxHeight: 200.0,
        child: Card(
          color: ConstantsColors.purple,
          elevation: 8.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Column(
            children: <Widget>[
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                          child: Text(
                        'Ficha tecnica de ingreso',
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: ConstantsColors.white),
                      )))),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: _buildInfoAdmission(admissionDataSheet)),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSilver(AdmissionDataSheet data) {
    return CustomScrollView(slivers: <Widget>[
      _infoCustomer(data.customer),
      _infoEquipment(data.equipment),
      _infoAdmission(data),
      _infoService(data.workServices),
    ]);
  }

  Column _buildInfoCustomer(Customer customer) {
    return Column(
      children: [
        _textAlign('Nombre: ' + customer.name + ' ' + customer.lastName),
        _textAlign(customer.typeDocument + ' ' + customer.document),
        _textAlign('Dirección: ' + customer.address),
        _textAlign('Email: ' + customer.email),
        _textAlign('Celular: ' + customer.phone),
      ],
    );
  }

  Column _buildInfoAdmission(AdmissionDataSheet admission) {
    return Column(
      children: [
        _textAlign('# ' + admission.id.toString()),
        _textAlign('Observaciones: ' + admission.observation),
        _textAlign('Causa: ' + admission.reason),
      ],
    );
  }

  Column _buildInfoEquipment(Equipment equipment) {
    return Column(children: [
      _textAlignEquipment('Marca: ', equipment.mark),
      _textAlignEquipment('Modelo: ', equipment.model),
      _textAlignEquipment('Disco duro: ', equipment.hardDisk),
      _textAlignEquipment('Memoria Ram: ', equipment.ram),
      _textAlignEquipment('Procesador: ', equipment.processor),
      _textAlignEquipment('Board: ', equipment.board),
      _textAlignEquipment('Unidad de CD: ', equipment.readerCd),
      _textAlignEquipment('Otros: ', equipment.other),
      _textAlignEquipment('Estado: ', equipment.status),
    ]);
  }

  Align _textAlign(String data) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        child: Text(
          data,
          style: TextStyle(fontSize: 16.0, color: ConstantsColors.white),
        ),
      ),
    );
  }

  Container _textAlignEquipment(String title, String data) {
    return Container(
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
                child: Column(
              children: [
                Text(
                  title,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: ConstantsColors.blackLight),
                ),
                Text(
                  data,
                  style: TextStyle(
                      fontSize: 16.0, color: ConstantsColors.blackLight),
                ),
                Divider(),
              ],
            )),
          ],
        ));
  }

  Widget _buildStreamD() {
    return StreamBuilder<List<Service>>(
        stream: _serviceBloc.optionsList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return dropdowns(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget dropdowns(List<Service> option) {
    return DropdownButton<Service>(
      isExpanded: true,
      value: option[0],
      icon: const Icon(Icons.arrow_downward),
      iconSize: 16,
      elevation: 16,
      style: const TextStyle(color: ConstantsColors.primaryColor),
      underline: Container(
        height: 2,
        color: ConstantsColors.secondaryColor,
      ),
      onChanged: (Service serviceSelect) {
        addWorkService(serviceSelect);
      },
      items: option.map<DropdownMenuItem<Service>>((Service value) {
        return DropdownMenuItem<Service>(
          value: value,
          child: Text(value.name),
        );
      }).toList(),
    );
  }

  void addWorkService(Service serviceSelect) {
    var workServiceNew = WorkService(
        services: serviceSelect,
        admissionDataSheet: _admissionDataSheetBloc.apiResponse.payload);

    _workServiceBloc.create(workServiceNew).then((value) {
      if (value.statusResponse == HttpStatus.created) {
        Navigator.of(context).pop();
        showDialogSuccess(context, 'Servicio agregado');
        _admissionDataSheetBloc.loadDataAdmission(id);
      } else {
        showAlert(context, 'Algo fallo');
      }
    }, onError: (err) {
      showAlert(context, 'Error Flutter');
    });
  }

  Widget _buildFormDialog(BuildContext context) {
    _serviceBloc
        .initializeDataDropDown(_admissionDataSheetBloc.apiResponse.payload);
    return AlertDialog(
      content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(bottom: 5), child: _buildStreamD())
          ]),
    );
  }
}
