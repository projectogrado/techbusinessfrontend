import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:techbusinessfrontend/src/bloc/auth_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/login_request_model.dart';
import 'package:techbusinessfrontend/src/model/success_login_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

class AuthUi extends StatefulWidget {
  const AuthUi({Key key}) : super(key: key);

  @override
  AuthUiState createState() => AuthUiState();
}

class AuthUiState extends State<AuthUi> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formLogin = GlobalKey<FormState>();
  final GlobalKey<FormState> _formForgot = GlobalKey<FormState>();

  AuthUiState();

  AuthBloc authBloc;
  LoginRequest loginRequestForm = LoginRequest();

  String username;
  bool _enableBtn = false;

  @override
  void dispose() {
    super.dispose();
    authBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    authBloc = AuthBloc();
  }

  void _submitResetPasswordRegisterForm() {
    final formForgot = _formForgot.currentState;

    if (formForgot.validate()) {
      formForgot.save();
      authBloc.recoverPassword(username).then((ApiResponse data) {
        if (data.statusResponse == 200) {
          _closeModal();
          showDialog(
            context: context,
            builder: (_) => _buildInfoDialog(context),
          );
        } else {
          _showErrorBackend(data);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  void _closeModal() {
    Navigator.of(context).pop();
  }

  void _submitLoginForm() {
    final formLogin = _formLogin.currentState;

    if (formLogin.validate()) {
      formLogin.save();
      authBloc.signIn(loginRequestForm).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.ok) {
          _switchUiByRole(data);
        } else if (data.statusResponse == HttpStatus.permanentRedirect) {
          TechBusinessNavigator.goToResetPassword(context, loginRequestForm.username, data.message);
        } else {
          _showErrorBackend(data);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  void _showErrorBackend(ApiResponse data) {
    ErrorApiResponse err = data.payload;
    showAlert(context, err.message);
  }

  void _switchUiByRole(ApiResponse data) {
    SuccessLogin login = data.payload;
    var payload = Jwt.parseJwt(login.token);
    if (payload['ROLES'][0] == 'ROLE_EMPLOYEE') {
      TechBusinessNavigator.goToEmployeeHomeUi(context);
    } else if (payload['ROLES'][0] == 'ROLE_ADMIN') {
      TechBusinessNavigator.goToAdministratorHomeUi(context);
    }
  }

  Widget _form(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Form(
          key: _formLogin,
          onChanged: () => setState(() => _enableBtn = _formLogin.currentState.validate()),
          child: Column(
            children: <Widget>[
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(icon: Icon(Icons.person), hintText: 'username'),
                onSaved: (String value) {
                  loginRequestForm.username = value;
                },
              ),
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                obscureText: true,
                decoration: InputDecoration(icon: Icon(Icons.no_encryption), hintText: 'password'),
                onSaved: (value) {
                  loginRequestForm.password = value;
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 48.0),
                child: AbsorbPointer(
                    absorbing: !_enableBtn,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: _enableBtn ? ConstantsColors.primaryColor : ConstantsColors.disableColor,
                          onPrimary: _enableBtn ? ConstantsColors.primaryColor : ConstantsColors.disableColor),
                      onPressed: () {
                        _submitLoginForm();
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
                        child: Text(
                          Constants.btnSignIn,
                          style: TextStyle(color: _enableBtn ? ConstantsColors.disableColor : ConstantsColors.lightGray),
                        ),
                      ),
                    )),
              ),
              TextButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (_) => _buildFormDialog(context),
                    );
                  },
                  child: Text('Has olvidado tu contraseña'))
            ],
          ),
        ));
  }

  Widget _loginForm(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
        child: Column(
      children: [
        SafeArea(
          child: Container(
            height: 180.0,
          ),
        ),
        Container(
          width: size.width * 0.85,
          margin: EdgeInsets.symmetric(vertical: 30.0),
          padding: EdgeInsets.symmetric(vertical: 50.0),
          decoration: BoxDecoration(
              color: ConstantsColors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(color: Colors.black26, blurRadius: 3.0, offset: Offset(0.0, 5.0), spreadRadius: 3.0)
              ]),
          child: Column(
            children: <Widget>[
              Text('Login', style: TextStyle(color: ConstantsColors.secondaryColor)),
              SizedBox(height: 60.0),
              _form(context),
              SizedBox(height: 100.0)
            ],
          ),
        ),
      ],
    ));
  }

  Widget _createBackground(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final background = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Theme.of(context).accentColor,
        ConstantsColors.secondaryColor,
      ])),
    );

    return Stack(
      children: <Widget>[
        background,
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(
                Icons.home_repair_service,
                color: ConstantsColors.moderateGray,
                size: 100,
              ),
              SizedBox(height: 10.0, width: double.infinity),
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        fit: StackFit.expand,
        children: [_createBackground(context), _loginForm(context)],
      ),
      backgroundColor: Colors.white60,
    );
  }

  Widget _buildInfoDialog(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Si la información suministrada es correcta, llegara un correo con una contraseña temporal'),
        ],
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                style:
                    ElevatedButton.styleFrom(primary: ConstantsColors.primaryColor, onPrimary: ConstantsColors.white),
                child: Text(Constants.btnAccept))
          ],
        )
      ],
    );
  }

  Widget _buildFormDialog(BuildContext context) {
    var title = 'Recuperar contraseña';
    return AlertDialog(
      title: Center(child: Text(title)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Ingrese el username para enviar un correo con una password temporal.'),
          Form(
            key: _formForgot,
            child: Column(
              children: <Widget>[
                TextFormField(
                  initialValue: '',
                  maxLength: 50,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  decoration: InputDecoration(hintText: Constants.labelUsername),
                  onSaved: (String value) {
                    username = value;
                  },
                ),
              ],
            ),
          )
        ],
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    Constants.btnCancel,
                    style: TextStyle(color: ConstantsColors.primaryColor),
                  ),
                )),
            ElevatedButton(
                onPressed: () {
                  _submitResetPasswordRegisterForm();
                },
                style:
                    ElevatedButton.styleFrom(primary: ConstantsColors.primaryColor, onPrimary: ConstantsColors.white),
                child: Text(Constants.btnSend))
          ],
        )
      ],
    );
  }
}
