import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:techbusinessfrontend/src/bloc/auth_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/recover_password.dart';
import 'package:techbusinessfrontend/src/model/success_login_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

class RestorePasswordUi extends StatefulWidget {
  final String username;
  final String role;

  const RestorePasswordUi({Key key, this.username, this.role}) : super(key: key);

  @override
  RestorePasswordUiState createState() => RestorePasswordUiState();
}

class RestorePasswordUiState extends State<RestorePasswordUi> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formRecover = GlobalKey<FormState>();

  RestorePasswordUiState();

  String username;
  String role;
  AuthBloc authBloc;
  RecoverPassword resetPassword = RecoverPassword();

  @override
  void dispose() {
    super.dispose();
    authBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    username = widget.username;
    role = widget.role;
    authBloc = AuthBloc();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => showDialogSuccess(context, 'Tu contraseña es temporal debes actualizarla'));
  }

  void _submitRecoverPasswordForm() {
    final formRecover = _formRecover.currentState;

    if (formRecover.validate()) {
      formRecover.save();
      setValueUser();
      authBloc.updatePassword(resetPassword).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.ok) {
          _switchUiByRole(data);
        } else {
          _showErrorBackend(data);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    } else {
      showAlert(context, 'El formulario no cumple con todas las validaciones');
    }
  }

  void _showErrorBackend(ApiResponse data) {
    ErrorApiResponse err = data.payload;
    showAlert(context, err.message);
  }

  void setValueUser() {
    resetPassword.username = username;
    resetPassword.role = role;
  }

  void _switchUiByRole(ApiResponse data) {
    SuccessLogin login = data.payload;
    var payload = Jwt.parseJwt(login.token);
    if (payload['ROLES'][0] == 'ROLE_EMPLOYEE') {
      TechBusinessNavigator.goToEmployeeHomeUi(context);
    } else if (payload['ROLES'][0] == 'ROLE_ADMIN') {
      TechBusinessNavigator.goToAdministratorHomeUi(context);
    }
  }

  Widget _form(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Form(
          key: _formRecover,
          child: Column(
            children: <Widget>[
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                obscureText: true,
                decoration: InputDecoration(icon: Icon(Icons.vpn_key_outlined), hintText: 'old password'),
                onSaved: (String value) {
                  resetPassword.oldPassword = value;
                },
              ),
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                obscureText: true,
                decoration: InputDecoration(icon: Icon(Icons.no_encryption), hintText: 'password'),
                onSaved: (value) {
                  resetPassword.password = value;
                },
              ),
              TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                obscureText: true,
                decoration: InputDecoration(icon: Icon(Icons.no_encryption), hintText: 'verifed password'),
                onSaved: (value) {
                  resetPassword.verifiedPassword = value;
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 48.0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: ConstantsColors.primaryColor, onPrimary: ConstantsColors.primaryColor),
                  onPressed: () {
                    _submitRecoverPasswordForm();
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
                    child: Text(
                      Constants.btnUpdates,
                      style: TextStyle(color: ConstantsColors.disableColor),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget _loginForm(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
        child: Column(
      children: [
        SafeArea(
          child: Container(
            height: 180.0,
          ),
        ),
        Container(
          width: size.width * 0.85,
          margin: EdgeInsets.symmetric(vertical: 30.0),
          padding: EdgeInsets.symmetric(vertical: 50.0),
          decoration: BoxDecoration(
              color: ConstantsColors.white,
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: <BoxShadow>[
                BoxShadow(color: Colors.black26, blurRadius: 3.0, offset: Offset(0.0, 5.0), spreadRadius: 3.0)
              ]),
          child: Column(
            children: <Widget>[
              Text('Recover password', style: TextStyle(color: ConstantsColors.secondaryColor)),
              SizedBox(height: 60.0),
              _form(context),
              // SizedBox(height: 100.0
            ],
          ),
        ),
      ],
    ));
  }

  Widget _createBackground(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final background = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Theme.of(context).accentColor,
        ConstantsColors.secondaryColor,
      ])),
    );

    return Stack(
      children: <Widget>[
        background,
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(
                Icons.ac_unit_sharp,
                color: ConstantsColors.moderateGray,
                size: 100,
              ),
              SizedBox(height: 10.0, width: double.infinity),
            ],
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        fit: StackFit.expand,
        children: [_createBackground(context), _loginForm(context)],
      ),
      backgroundColor: Colors.white60,
    );
  }
}
