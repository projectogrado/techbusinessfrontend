import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:techbusinessfrontend/src/bloc/person_receive_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/discharge_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/person_receive_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';

class PersonReceiveUi extends StatefulWidget {
  final DischargeDataSheet dischargeDataSheet;

  const PersonReceiveUi({Key key, this.dischargeDataSheet}) : super(key: key);

  @override
  PersonReceiveUiState createState() => PersonReceiveUiState();
}

class PersonReceiveUiState extends State<PersonReceiveUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formRegister = GlobalKey<FormState>();

  PersonReceiveUiState();

  DischargeDataSheet dischargeDataSheet;
  PersonReceiveBloc personReceiveBloc;
  PersonReceive personReceive = PersonReceive();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    personReceiveBloc = PersonReceiveBloc();
    dischargeDataSheet = widget.dischargeDataSheet;
  }

  void _submitRegisterForm() {
    final formRegister = _formRegister.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      personReceive.dischargeDataSheet = dischargeDataSheet;
      personReceiveBloc.create(personReceive).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.created) {
          TechBusinessNavigator.goEmployeeHome(context);
          showDialogSuccess(context, 'Guardado exitoso');
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text(Constants.appBarRegisterPersonReceive),
          backgroundColor: ConstantsColors.primaryColor,
        ),
        body: SingleChildScrollView(
            child: ConstrainedBox(
          constraints:
              BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Stack(children: [
            Padding(padding: const EdgeInsets.all(8.0), child: _buildForm()),
          ]),
        )));
  }

  Form _buildForm() {
    return Form(
      key: _formRegister,
      child: Column(children: <Widget>[
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.drive_file_rename_outline),
                    hintText: 'Name'),
                onSaved: (String value) {
                  personReceive.name = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.drive_file_rename_outline),
                    hintText: 'Lastname'),
                onSaved: (String value) {
                  personReceive.lastName = value;
                })),
        SelectFormField(
            type: SelectFormFieldType.dropdown,
            initialValue: 'CC',
            icon: Icon(Icons.format_shapes),
            labelText: 'Type Document ',
            items: Constants.itemSelectTypeDocument,
            onChanged: print,
            onSaved: (String value) {
              personReceive.typeDocument = value;
            }),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.card_membership),
                    hintText: 'Document'),
                onSaved: (String value) {
                  personReceive.document = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email_outlined), hintText: 'Email'),
                onSaved: (String value) {
                  personReceive.email = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.roofing_outlined),
                    hintText: 'Address'),
                onSaved: (String value) {
                  personReceive.address = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.phone_android), hintText: 'Phone'),
                onSaved: (String value) {
                  personReceive.phone = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.supervisor_account_sharp),
                    hintText: 'Kinship'),
                onSaved: (String value) {
                  personReceive.kinship = value;
                })),
        SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: ConstantsColors.primaryColor),
              onPressed: () {
                _submitRegisterForm();
              },
              child: Text('Register'),
            ))
      ]),
    );
  }
}
