import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/model/discharge_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

class DischargeDataSheetUi extends StatefulWidget {
  final dischargeDataSheet;

  const DischargeDataSheetUi({Key key, this.dischargeDataSheet})
      : super(key: key);

  @override
  DischargeDataSheetUiState createState() => DischargeDataSheetUiState();
}

class DischargeDataSheetUiState extends State<DischargeDataSheetUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  DischargeDataSheetUiState();

  DischargeDataSheet dischargeDataSheet;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    dischargeDataSheet = widget.dischargeDataSheet;
  }

  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text(Constants.labelDischargeDataSheet),
        backgroundColor: ConstantsColors.primaryColor,
        actions: [
          IconButton(icon: Icon(Icons.send_sharp), onPressed: () {
            TechBusinessNavigator.goToDeliveryPerson(context, dischargeDataSheet);
          },)
        ],
      ),
      body: _buildStream(),
    );
  }

  Widget _buildStream() {
    return buildInfoDischargeDataSheet(dischargeDataSheet);
  }

  Widget buildInfoDischargeDataSheet(DischargeDataSheet data) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Align(
          child: SizedBox(
            width: 400.0,
            height: 800.0,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Card(
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                    ListTile(
                        leading: Icon(Icons.album),
                        title: Text(data.id.toString()),
                        subtitle: Text(data.deliverDate))
                  ])),
                  Card(
                    elevation: 50,
                    shadowColor: ConstantsColors.black,
                    child: Container(
                      padding: EdgeInsets.all(32.0),
                      width: 600.0,
                      child: Column(
                        children: <Widget>[
                          Text(
                            'Service',
                            style: TextStyle(
                              fontSize: 30,
                              color: ConstantsColors.green,
                              fontWeight: FontWeight.w500,
                            ),
                            //Textstyle
                          ),
                          Text(
                            data.admissionDataSheet.workServices[0].services
                                .name,
                            style: TextStyle(
                              fontSize: 15,
                              color: ConstantsColors.green,
                              fontWeight: FontWeight.w500,
                            ), //Textstyle
                          ),
                          Text(
                            data.admissionDataSheet.workServices[0].services
                                .description,
                            style: TextStyle(
                              fontSize: 15,
                              color: ConstantsColors.green,
                              fontWeight: FontWeight.w500,
                            ), //Textstyle
                          ),
                        ],
                      ),
                    ),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: data.admissionDataSheet.workServices[0]
                        .checklistRepairs.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        elevation: 50,
                        shadowColor: Colors.black,
                        child: Container(
                          padding: EdgeInsets.all(32.0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                'check list',
                                style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ),
                                //Textstyle
                              ),
                              Text(
                                data.admissionDataSheet.workServices[0]
                                    .checklistRepairs[index].evidence,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                              Text(
                                data.admissionDataSheet.workServices[0]
                                    .checklistRepairs[index].status
                                    .toString(),
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  Divider()
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
