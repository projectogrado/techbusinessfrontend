import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/admission_data_sheet_bloc.dart';
import 'package:techbusinessfrontend/src/bloc/employee_bloc.dart';
import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/ui/employee/widget/navigator_employee.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/custom_list_item.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';
import 'package:techbusinessfrontend/src/utils/widgets/notification_badges.dart';

import '../../navegator_techbusiness.dart';

class EmployeeHomeUi extends StatefulWidget {
  @override
  EmployeeHomeUiState createState() => EmployeeHomeUiState();
}

class EmployeeHomeUiState extends State<EmployeeHomeUi>
    with SingleTickerProviderStateMixin {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Employee me;

  String tokenFirebase;
  int counter = 0;
  EmployeeBloc employeeBloc;
  AdmissionDataSheetBloc admissionDataSheetBloc;
  StreamSubscription<Employee> streamSubscription;

  @override
  void dispose() {
    super.dispose();
    streamSubscription.cancel();
    employeeBloc.dispose();
    admissionDataSheetBloc.dispose();
  }

  @override
  void initState() {
    _registerOnFirebase();
    getMessage();
    super.initState();
    employeeBloc = EmployeeBloc();
    admissionDataSheetBloc = AdmissionDataSheetBloc();
  }

  void _registerOnFirebase() {
    _firebaseMessaging.subscribeToTopic('all');
    _firebaseMessaging.getToken().then((token) => {tokenFirebase = token});
  }

  void getMessage() {
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          setState(() => counter++);
        },
        onResume: (Map<String, dynamic> message) async {},
        onLaunch: (Map<String, dynamic> message) async {});
  }

  void _initial() {
    employeeBloc.getWorks().then((ApiResponse value) {
      me = value.payload;
      if (me.tokenNotification != null && me.tokenNotification.isNotEmpty) {
        if (me.tokenNotification != tokenFirebase) {
          me.tokenNotification = tokenFirebase;
          employeeBloc.update(me);
        }
      } else {
        me.tokenNotification = tokenFirebase;
        employeeBloc.update(me);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    _initial();
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text('¡Hola!'),
          actions: [
            NotificationBadges(
              counter: counter,
              resetBadge: () {
                setState(() => counter = 0);
              },
            ),
            IconButton(
              icon: Icon(
                Icons.account_circle,
              ),
              onPressed: () {
                setState(() => counter++);
              },
            ),
          ],
          bottom: PreferredSize(
              preferredSize: Size.fromHeight(30.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Text('Bienvendio',
                            style: TextStyle(color: ConstantsColors.white))),
                    Container(
                      width: size.width * 0.25,
                    ),
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: GestureDetector(
                          onTap: () {
                            _showAlertDialog();
                          },
                          child: Icon(
                            Icons.logout,
                            size: 24,
                            color: ConstantsColors.white,
                          )),
                    )
                  ])),
        ),
        body: Stack(children: [
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              child: _buildStream())
        ]),
        bottomNavigationBar: EmployeeNavigator(
          currentIndex: 0,
          actionAdmission: () {
            TechBusinessNavigator.goToAdmissionDataSheetUi(context);
          },
          actionDelivery: () {
            // TechBusinessNavigator.goToDischargeDataSheet(context);
          },
          actionHome: () {
            _initial();
          },
        ));
  }

  Widget _buildStream() {
    return StreamBuilder<List<AdmissionDataSheet>>(
        stream: employeeBloc.workList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget buildList(List<AdmissionDataSheet> data) {
    return CustomScrollView(
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return Column(
              children: [buildStartComponent(data[index], context), Divider()]);
        }, childCount: data.length))
      ],
    );
  }

  CustomListItem buildStartComponent(
      AdmissionDataSheet admissionDataSheet, BuildContext context) {
    if (admissionDataSheet.equipment.status == 'TO_DO') {
      return CustomListItem(
          title: admissionDataSheet.id.toString(),
          description: admissionDataSheet.observation,
          status: admissionDataSheet.reason,
          actionShow: () {
            TechBusinessNavigator.goToAdmissionDetail(
                context, admissionDataSheet.id);
          },
          actionStart: () {
            admissionDataSheetBloc.updateStatusAdmission(
                admissionDataSheet.id, 'IN_REPAIR');
            TechBusinessNavigator.goToRouteWork(context, admissionDataSheet.id);
          },
          actionClimb: () {
            _climb(admissionDataSheet.id);
          });
    } else if (admissionDataSheet.equipment.status == 'NOVELTY' ||
        admissionDataSheet.equipment.status == 'IN_REPAIR') {
      return CustomListItem(
          title: admissionDataSheet.id.toString(),
          description: admissionDataSheet.observation,
          status: admissionDataSheet.reason,
          actionShow: () {
            TechBusinessNavigator.goToAdmissionDetail(
                context, admissionDataSheet.id);
          },
          actionContinue: () {
            TechBusinessNavigator.goToRouteWork(context, admissionDataSheet.id);
          },
          actionClimb: () {
            _climb(admissionDataSheet.id);
          });
    } else {
      return CustomListItem(
          title: admissionDataSheet.id.toString(),
          description: admissionDataSheet.observation,
          status: admissionDataSheet.reason,
          actionShow: () {
            TechBusinessNavigator.goToAdmissionDetail(
                context, admissionDataSheet.id);
          });
    }
  }

  void _climb(int id) {
    admissionDataSheetBloc.climbAdmission(id).then((ApiResponse value) {
      if (value.statusResponse == HttpStatus.ok) {
        _initial();
      } else {
        showAlert(context, 'Error');
      }
    }, onError: (err) {
      showAlert(context, 'Error');
    });
  }

  void _showAlertDialog() {
    Widget cancel = TextButton(
      onPressed: () {
        Navigator.pop(context);
      },
      child: Text(Constants.btnCancel),
    );

    Widget accept = TextButton(
      onPressed: () {
        TechBusinessNavigator.goToAuthUi(context);
      },
      child: Text(Constants.btnAccept),
    );

    var alert = AlertDialog(
      title: Text(
        Constants.logout,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            Text(Constants.messageLogoutConfirm,
                style: Theme.of(context).textTheme.bodyText2),
          ],
        ),
      ),
      actions: [
        cancel,
        accept,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
