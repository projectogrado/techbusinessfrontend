import 'dart:io';

import 'package:camera/camera.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:string_validator/string_validator.dart';
import 'package:techbusinessfrontend/src/bloc/work_serivice_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/checklist_repair_key.dart';
import 'package:techbusinessfrontend/src/model/checklist_repair_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';
import 'package:techbusinessfrontend/src/utils/widgets/take_picture_page.dart';

class RouteWorkDetailUi extends StatefulWidget {
  final int id;
  final WorkService workService;

  const RouteWorkDetailUi({Key key, this.id, this.workService})
      : super(key: key);

  @override
  RouteWorkDetailUiState createState() => RouteWorkDetailUiState();
}

class RouteWorkDetailUiState extends State<RouteWorkDetailUi>
    with SingleTickerProviderStateMixin {
  RouteWorkDetailUiState();

  int id;
  WorkServiceBloc workServiceBloc;
  WorkService workService;
  final picker = ImagePicker();
  List<Step> steps;

  @override
  void initState() {
    super.initState();
    workServiceBloc = WorkServiceBloc();
    workService = widget.workService;
    id = widget.id;
    loadWorkService();
  }

  void loadWorkService() {
    if (workService.checklistRepairs.isEmpty) {
      workService.services.repairRoute.forEach((checklist) {
        workService.checklistRepairs.add(ChecklistRepair(
            id: ChecklistRepairKey(
                workServiceId: workService.id, repairRouteId: checklist.id)));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Constants.appBarWorkRoute,
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: ConstantsColors.lightGray)),
        centerTitle: true,
        elevation: 0.0,
      ),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: ConstantsColors.purple,
        foregroundColor: ConstantsColors.lightGray,
        icon: Icon(
          Icons.check,
          size: 24,
        ),
        label: Text('Guardar'),
        onPressed: () {
          _save();
        },
      ),
      body: buildList(workService.checklistRepairs),
    );
  }

  void _save() {
    uploadImage(workService).then((value) {
      _saveData();
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  void _saveData() {
    workServiceBloc
        .addCheckLit(workService.checklistRepairs, workService.id)
        .then((ApiResponse data) {
      if (data.statusResponse == HttpStatus.ok) {
        TechBusinessNavigator.goBack(context);
        workServiceBloc.loadWorkServiceById(id);
        showDialogSuccess(context, 'Guardado exitoso');
      } else {
        ErrorApiResponse err = data.payload;
        showAlert(context, err.message);
      }
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  bool notNull(Object o) => o != null;

  ExpansionTile buildListItem(ChecklistRepair checklistRepair) {
    return ExpansionTile(
      trailing: Icon(
        Icons.keyboard_arrow_down_outlined,
        size: 24,
      ),
      backgroundColor: ConstantsColors.veryLightGray,
      title: CheckboxListTile(
        controlAffinity: ListTileControlAffinity.leading,
        title: Text(checklistRepair.repairRoute.process.name,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18.0,
            )),
        value: checklistRepair.status,
        onChanged: (bool value) {
          setState(() {
            checklistRepair.status = value;
          });
        },
      ),
      children: [
        Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(checklistRepair.repairRoute.process.description),
                IconButton(
                    icon: Icon(Icons.add_a_photo_outlined),
                    onPressed: () {
                      _showOptions(context, checklistRepair);
                    }),
                checklistRepair.evidence != null
                    ? getImageFile(checklistRepair.evidence)
                    : null
              ].where(notNull).toList(),
            )),
      ],
    );
  }

  Image getImageFile(String evidence) {
    return isURL(evidence)
        ? Image.network(evidence, height: 150, width: 200)
        : Image.file(File(evidence), height: 150, width: 200);
  }

  Widget buildList(List<ChecklistRepair> data) {
    return CustomScrollView(
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return Column(children: [buildListItem(data[index]), Divider()]);
        }, childCount: data.length))
      ],
    );
  }

  void _showCamera(ChecklistRepair checklistRepair) async {
    final cameras = await availableCameras();
    final camera = cameras.first;

    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TakePicturePage(camera: camera)));

    setState(() {
      checklistRepair.evidence = result;
    });
  }

  void _showOptions(BuildContext context, ChecklistRepair checklistRepair) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                    onTap: () {
                      Navigator.pop(context);
                      _showCamera(checklistRepair);
                    },
                    leading: Icon(
                      Icons.photo_camera,
                      size: 24,
                    ),
                    title: Text('Take a picture from camera')),
                ListTile(
                    onTap: () {
                      Navigator.pop(context);
                      _showPhotoLibrary(checklistRepair);
                    },
                    leading: Icon(Icons.photo_library, size: 24),
                    title: Text('Choose from photo library'))
              ]));
        });
  }

  void _showPhotoLibrary(ChecklistRepair checklistRepair) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        checklistRepair.evidence = pickedFile.path;
      } else {
        // ignore: avoid_print
        print('No image selected.');
      }
    });
  }

  Future<void> uploadFile(ChecklistRepair checklistRepair) async {
    var file = File(checklistRepair.evidence);
    var name = 'evidence/evidence_' + DateTime.now().toString() + '.png';
    try {
      var storageReference = FirebaseStorage.instance.ref().child(name);
      await storageReference.putFile(file);
      await storageReference.getDownloadURL().then((value) {
        checklistRepair.evidence = value;
      });
    // ignore: empty_catches
    } on FirebaseException {
    }
  }

  Future<void> uploadImage(WorkService workService) async {
    workService.checklistRepairs.forEach((element) async {
      if (null != element.evidence && !isURL(element.evidence)) {
        await uploadFile(element);
      }
    });
  }
}
