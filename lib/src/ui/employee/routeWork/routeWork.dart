import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:techbusinessfrontend/src/bloc/admission_data_sheet_bloc.dart';
import 'package:techbusinessfrontend/src/bloc/work_serivice_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/custom_list_route.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';

import '../../../navegator_techbusiness.dart';

class RouteWorkUi extends StatefulWidget {
  final int admissionId;

  const RouteWorkUi({Key key, this.admissionId}) : super(key: key);

  @override
  RouteWorkUiState createState() => RouteWorkUiState();
}

class RouteWorkUiState extends State<RouteWorkUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  RouteWorkUiState();

  int id;
  AdmissionDataSheetBloc admissionDataSheetBloc;
  WorkServiceBloc workServiceBloc;
  bool isSelected = false;
  bool isReady = false;

  @override
  void dispose() {
    super.dispose();
    workServiceBloc.dispose();
    admissionDataSheetBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    workServiceBloc = WorkServiceBloc();
    admissionDataSheetBloc = AdmissionDataSheetBloc();
    id = widget.admissionId;
    SchedulerBinding.instance
        .addPostFrameCallback((_) => workServiceBloc.loadWorkServiceById(id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(Constants.appBarWorkRoute,
              style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  color: ConstantsColors.lightGray)),
          centerTitle: true,
          elevation: 0.0,
          actions: [
            IconButton(
                icon: Icon(Icons.refresh),
                onPressed: () {
                  workServiceBloc.loadWorkServiceById(id);
                })
          ],
        ),
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: ConstantsColors.purple,
          foregroundColor: ConstantsColors.lightGray,
          label: Text('Terminar'),
          onPressed: () {
            _save();
          },
        ),
        body: Stack(children: [
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              child: _buildStream())
        ]));
  }

  Widget _buildStream() {
    return StreamBuilder<List<WorkService>>(
        stream: workServiceBloc.workServiceStream,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return _buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget _buildList(List<WorkService> data) {
    return CustomScrollView(
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return Column(children: [buildItem(data[index], context), Divider()]);
        }, childCount: data.length))
      ],
    );
  }

  CustomListItemRoute buildItem(WorkService data, BuildContext context) {
    return CustomListItemRoute(
      icon: Icon(Icons.keyboard_arrow_right_outlined),
      title: data.services.name,
      description: data.services.description,
      status: data.ready.toString(),
      actionShow: () {
        TechBusinessNavigator.goToRouteWorkDetail(context, data, id);
      },
    );
  }

  void _save() {
    var modal = <Widget>[
      Text('Esta por terminar la reparación, el equipo fue reparado ?'),
      Divider(),
      Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  _changeStatus('REPAIRED');
                },
                child: Text('Reparado')),
            ElevatedButton(
                onPressed: () {
                  _changeStatus('NOT_REPAIRED');
                },
                style: ElevatedButton.styleFrom(primary: ConstantsColors.error),
                child: Text('No Reparado')),
          ])
    ];
    workServiceBloc.loadWorkServiceById(id).then((ApiResponse value) {
      List<WorkService> list = value.payload;
      if (list.where((element) => element.ready == false).isEmpty) {
        showCustomModalBottomSheet(context, modal, 100);
      } else {
        showWarning(context, 'Advertencia',
            'verifique que todos los procesos han sido checkeados');
      }
    });
  }

  void _changeStatus(String status) {
    admissionDataSheetBloc.updateStatusAdmission(id, status).then(
        (ApiResponse value) {
      if (value.statusResponse == HttpStatus.ok) {
        TechBusinessNavigator.goEmployeeHome(context);
        showDialogSuccess(context, value.message);
      } else {
        showAlert(context, value.message);
      }
    }, onError: (err) {
      showAlert(context, err);
    });
  }
}
