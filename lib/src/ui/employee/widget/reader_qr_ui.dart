import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:techbusinessfrontend/src/bloc/dischargedatasheet_bloc.dart';
import 'package:techbusinessfrontend/src/model/discharge_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

class ReaderQrUi extends StatefulWidget {
  @override
  ReaderQrUiState createState() => ReaderQrUiState();
}

class ReaderQrUiState extends State<ReaderQrUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ScanResult scanResult;
  final _flashOnController = TextEditingController(text: Constants.flashOn);
  final _flashOffController = TextEditingController(text: Constants.flashOff);
  final _cancelController = TextEditingController(text: Constants.flashCancel);
  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);
  List<BarcodeFormat> selectedFormats = [..._possibleFormats];
  DischargeDataSheetBloc _dischargeDataSheetBloc;
  List<DischargeDataSheet> listDischargeDataSheet = [];
  String message;
  final _aspectTolerance = 0.00;
  final _selectedCamera = -1;
  final _useAutoFocus = true;
  final _autoEnableFlash = false;

  @override
  // ignore: always_declare_return_types
  initState() {
    super.initState();

    Future.delayed(Duration.zero, () async {
      setState(() {});
    });
    _dischargeDataSheetBloc = DischargeDataSheetBloc();
  }

  void _submitConsulAdmission() {
    var ulr = scanResult.rawContent;
    var data = ulr.split('/');
    if (data[4] != null) {
      _dischargeDataSheetBloc.initializeData(data[4]).then((apiResponse) {
        if (apiResponse.statusResponse != 200) {
          message = Constants.errorQr;
          showErrorQrDialog(context, message);
        }
      });
    } else {
      message = Constants.errorQr;
      showErrorQrDialog(context, message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(Constants.appBarDichargeDataSheet),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.camera),
            tooltip: Constants.scanToolTip,
            onPressed: scan,
          )
        ],
      ),
      body: _buildStream(),
    );
  }

  Widget _buildStream() {
    return StreamBuilder<DischargeDataSheet>(
        stream: _dischargeDataSheetBloc.dischargeDataSheet,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildInfoDischargeDataSheet(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget buildInfoDischargeDataSheet(DischargeDataSheet data) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Align(
          child: SizedBox(
            width: 400.0,
            height: 800.0,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Card(
                      child: Column(mainAxisSize: MainAxisSize.min, children: [
                    ListTile(
                        leading: Icon(Icons.album),
                        title: Text(data.id.toString()),
                        subtitle: Text(data.deliverDate))
                  ])),
                  Card(
                    elevation: 50,
                    shadowColor: ConstantsColors.black,
                    child: Container(
                      padding: EdgeInsets.all(32.0),
                      width: 600.0,
                      child: Column(
                        children: <Widget>[
                          Text(
                            'Service',
                            style: TextStyle(
                              fontSize: 30,
                              color: ConstantsColors.green,
                              fontWeight: FontWeight.w500,
                            ),
                            //Textstyle
                          ),
                          Text(
                            data.admissionDataSheet.workServices[0].services
                                .name,
                            style: TextStyle(
                              fontSize: 15,
                              color: ConstantsColors.green,
                              fontWeight: FontWeight.w500,
                            ), //Textstyle
                          ),
                          Text(
                            data.admissionDataSheet.workServices[0].services
                                .description,
                            style: TextStyle(
                              fontSize: 15,
                              color: ConstantsColors.green,
                              fontWeight: FontWeight.w500,
                            ), //Textstyle
                          ),
                        ],
                      ),
                    ),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: data.admissionDataSheet.workServices[0]
                        .checklistRepairs.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        elevation: 50,
                        shadowColor: Colors.black,
                        child: Container(
                          padding: EdgeInsets.all(32.0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                'check list',
                                style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ),
                                //Textstyle
                              ),
                              Text(
                                data.admissionDataSheet.workServices[0]
                                    .checklistRepairs[index].evidence,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                              Text(
                                data.admissionDataSheet.workServices[0]
                                    .checklistRepairs[index].status
                                    .toString(),
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  Divider()
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future scan() async {
    try {
      var options = ScanOptions(
        strings: {
          Constants.flashCancel: _cancelController.text,
          Constants.flashOn: _flashOnController.text,
          Constants.flashOff: _flashOffController.text,
        },
        restrictFormat: selectedFormats,
        useCamera: _selectedCamera,
        autoEnableFlash: _autoEnableFlash,
        android: AndroidOptions(
          aspectTolerance: _aspectTolerance,
          useAutoFocus: _useAutoFocus,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        var message = Constants.permissiondeDenied;
        showErrorQrDialog(context, message);
      } else {
        message = 'Unknown error: $e';
        showErrorQrDialog(context, message);
      }
      setState(() {
        scanResult = result;
      });
    }

    _submitConsulAdmission();
  }

  void showErrorQrDialog(BuildContext context, String message) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title:
                Row(children: [Icon(Icons.info), Text(Constants.tittleDialog)]),
            content: Text(message),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(Constants.btnAccept),
              )
            ],
          );
        });
  }
}
