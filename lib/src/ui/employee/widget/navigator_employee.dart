import 'package:barcode_scan/barcode_scan.dart';
import 'package:barcode_scan/model/scan_result.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:techbusinessfrontend/src/model/discharge_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/bloc/dischargedatasheet_bloc.dart';

class EmployeeNavigator extends StatefulWidget {
  final int currentIndex;
  final VoidCallback actionHome;
  final VoidCallback actionDelivery;
  final VoidCallback actionAdmission;

  EmployeeNavigator(
      {Key key,
      this.currentIndex,
      this.actionHome,
      this.actionDelivery,
      this.actionAdmission})
      : super(key: key);

  @override
  EmployeeNavigatorState createState() => EmployeeNavigatorState();
}

class EmployeeNavigatorState extends State<EmployeeNavigator>
    with SingleTickerProviderStateMixin {
  DischargeDataSheetBloc _dischargeDataSheetBloc;

  ScanResult scanResult;
  final _flashOnController = TextEditingController(text: Constants.flashOn);
  final _flashOffController = TextEditingController(text: Constants.flashOff);
  final _cancelController = TextEditingController(text: Constants.flashCancel);
  int currentIndex;
  VoidCallback actionHome;
  VoidCallback actionDelivery;
  VoidCallback actionAdmission;
  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);
  List<BarcodeFormat> selectedFormats = [..._possibleFormats];
  List<DischargeDataSheet> listDischargeDataSheet = [];
  String message;
  final _aspectTolerance = 0.00;
  final _selectedCamera = -1;
  final _useAutoFocus = true;
  final _autoEnableFlash = false;

  @override
  void dispose() {
    super.dispose();
    _dischargeDataSheetBloc.dispose();
  }


  @override
  void initState() {
    super.initState();
    currentIndex = widget.currentIndex;
    actionHome = widget.actionHome;
    actionDelivery = widget.actionDelivery;
    actionAdmission = widget.actionAdmission;
    _dischargeDataSheetBloc = DischargeDataSheetBloc();

    Future.delayed(Duration.zero, () async {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return navigatorBar(context);
  }

  Widget navigatorBar(context) {
    final size = MediaQuery.of(context).size;

    return Container(
        color: Colors.transparent,
        width: size.width,
        height: 80,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            CustomPaint(
              size: Size(size.width, 80),
              painter: BNBCustomPainter(),
            ),
            Center(
              heightFactor: 0.6,
              child: FloatingActionButton(
                  backgroundColor: ConstantsColors.secondaryColor,
                  elevation: 0.1,
                  onPressed: actionAdmission,
                  child: Icon(
                    Icons.web_outlined,
                    size: 24,
                  )),
            ),
            Container(
              width: size.width,
              height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.home,
                      color: currentIndex == 0
                          ? ConstantsColors.white
                          : ConstantsColors.normalGray,
                    ),
                    onPressed: actionHome,
                    splashColor: ConstantsColors.white,
                  ),
                  Container(
                    width: size.width * 0.40,
                  ),
                  IconButton(
                      icon: Icon(
                        Icons.backpack_outlined,
                        color: currentIndex == 3
                            ? ConstantsColors.white
                            : ConstantsColors.normalGray,
                      ),
                      onPressed: scan),
                ],
              ),
            )
          ],
        ));
  }

  Future scan() async {
    try {
      var options = ScanOptions(
        strings: {
          Constants.flashCancel: _cancelController.text,
          Constants.flashOn: _flashOnController.text,
          Constants.flashOff: _flashOffController.text,
        },
        restrictFormat: selectedFormats,
        useCamera: _selectedCamera,
        autoEnableFlash: _autoEnableFlash,
        android: AndroidOptions(
          aspectTolerance: _aspectTolerance,
          useAutoFocus: _useAutoFocus,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        var message = Constants.permissiondeDenied;
        showErrorQrDialog(context, message);
      } else {
        message = 'Unknown error: $e';
        showErrorQrDialog(context, message);
      }
      setState(() {
        scanResult = result;
      });
    }

    _submitConsulAdmission();
  }

  void _submitConsulAdmission() {
    var ulr = scanResult.rawContent;
    var data = ulr.split('/');
    if (data[4] != null) {
      _dischargeDataSheetBloc.initializeData(data[4]).then((apiResponse) {
        if (apiResponse.statusResponse == 200) {
          TechBusinessNavigator.goToDischargeDataSheet(context, apiResponse.payload);

        }else{
          message = Constants.errorQr;
          showErrorQrDialog(context, message);
        }
      });
    } else {
      message = Constants.errorQr;
      showErrorQrDialog(context, message);
    }
  }

  void showErrorQrDialog(BuildContext context, String message) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title:
                Row(children: [Icon(Icons.info), Text(Constants.tittleDialog)]),
            content: Text(message),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(Constants.btnAccept),
              )
            ],
          );
        });
  }
}

class BNBCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = ConstantsColors.secondaryColor
      ..style = PaintingStyle.fill;

    var path = Path();
    path.moveTo(0, 20); // Start
    path.quadraticBezierTo(size.width * 0.20, 0, size.width * 0.35, 0);
    path.quadraticBezierTo(size.width * 0.40, 0, size.width * 0.40, 20);
    path.arcToPoint(Offset(size.width * 0.60, 20),
        radius: Radius.circular(20.0), clockwise: false);
    path.quadraticBezierTo(size.width * 0.60, 0, size.width * 0.65, 0);
    path.quadraticBezierTo(size.width * 0.80, 0, size.width, 20);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.lineTo(0, 20);
    canvas.drawShadow(path, Colors.black, 5, true);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
