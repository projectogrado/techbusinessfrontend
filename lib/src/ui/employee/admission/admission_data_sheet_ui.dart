import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:techbusinessfrontend/src/bloc/admission_data_sheet_bloc.dart';
import 'package:techbusinessfrontend/src/bloc/customer_bloc.dart';
import 'package:techbusinessfrontend/src/model/admission_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/customer_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/ui/employee/widget/navigator_employee.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constans_status.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';
import 'package:techbusinessfrontend/src/utils/widgets/notification_badges.dart';

class AdmissionDataSheetUi extends StatefulWidget {
  @override
  AdmissionDataSheetUiState createState() => AdmissionDataSheetUiState();
}

class AdmissionDataSheetUiState extends State<AdmissionDataSheetUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  AdmissionDataSheet admissionDataSheet = AdmissionDataSheet();
  AdmissionDataSheetBloc _admissionDataSheetBloc;
  CustomerBloc _customerBloc;
  Customer customer = Customer();
  Equipment equipment = Equipment();
  final _controllerDocument = TextEditingController();
  final _controllerName = TextEditingController();
  final _controllerLastName = TextEditingController();
  final _controllerEmail = TextEditingController();
  final _controllerAddress = TextEditingController();
  final _controllerPhone = TextEditingController();
  var counter = 0;
  var currStep = 0;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _customerBloc = CustomerBloc();
    _admissionDataSheetBloc = AdmissionDataSheetBloc();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text('¡Hola!'),
          actions: [
            NotificationBadges(
              counter: counter,
              resetBadge: () {
                setState(() {
                  counter = 0;
                });
              },
            ),
            IconButton(
              icon: Icon(
                Icons.account_circle,
              ),
              onPressed: () {
                setState(() {
                  counter++;
                });
              },
            ),
          ],
          bottom: PreferredSize(
              preferredSize: Size.fromHeight(30.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Text('Bienvendio',
                            style: TextStyle(color: ConstantsColors.white))),
                    Container(
                      width: size.width * 0.25,
                    ),
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: GestureDetector(
                          onTap: () {
                            _showAlertDialog();
                          },
                          child: Icon(
                            Icons.logout,
                            size: 24,
                            color: ConstantsColors.white,
                          )),
                    )
                  ])),
        ),
        body: SingleChildScrollView(
            child: ConstrainedBox(
                constraints: BoxConstraints(
                    maxHeight: MediaQuery.of(context).size.height),
                child: Stack(children: [
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Form(
                          key: _formKey,
                          child: Expanded(
                              child: Stepper(
                                  steps: _buildSteps(),
                                  physics: ScrollPhysics(),
                                  type: StepperType.vertical,
                                  currentStep: currStep,
                                  onStepContinue:
                                      currStep < 2 ? continued : null,
                                  onStepCancel: currStep < 2 ? cancel : null,
                                  onStepTapped: tapped,
                                  controlsBuilder: (BuildContext context,
                                      {VoidCallback onStepContinue,
                                      VoidCallback onStepCancel}) {
                                    return Row(children: [
                                      onStepContinue != null
                                          ? TextButton(
                                              onPressed: onStepContinue,
                                              child: Text('Siguiente'))
                                          : Text(''),
                                      onStepCancel != null
                                          ? TextButton(
                                              onPressed: onStepCancel,
                                              child: Text('Volver'))
                                          : Text('')
                                    ]);
                                  }))))
                ]))),
        bottomNavigationBar: EmployeeNavigator(
          currentIndex: 1,
          actionAdmission: () {
            TechBusinessNavigator.goToAdmissionDataSheetUi(context);
          },
          actionDelivery: () {
            // TechBusinessNavigator.goToDischargeDataSheet(context);
          },
          actionHome: () {
            TechBusinessNavigator.goToEmployeeHomeUi(context);
          },
        ));
  }

  List<Step> _buildSteps() {
    return <Step>[
      Step(
          title: const Text('Customer', style: TextStyle(fontSize: 12)),
          subtitle: const Text('Form', style: TextStyle(fontSize: 10)),
          isActive: true,
          //state: StepState.editing,
          state: StepState.indexed,
          content: Column(
            children: [
              SelectFormField(
                  type: SelectFormFieldType.dropdown,
                  initialValue: 'CC',
                  labelText: 'Type Document ',
                  items: Constants.itemSelectTypeDocument,
                  onChanged: print,
                  onSaved: (String value) {
                    customer.typeDocument = value;
                  }),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      controller: _controllerDocument,
                      decoration: InputDecoration(
                          hintText: 'Document',
                          suffixIcon: IconButton(
                            onPressed: () =>
                                _getCustomer(_controllerDocument.value.text),
                            icon: Icon(Icons.person_search_outlined),
                          )),
                      onSaved: (String value) {
                        customer.document = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      controller: _controllerName,
                      decoration: InputDecoration(hintText: 'Name'),
                      onSaved: (String value) {
                        customer.name = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      controller: _controllerLastName,
                      decoration: InputDecoration(hintText: 'Lastname'),
                      onSaved: (String value) {
                        customer.lastName = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      controller: _controllerEmail,
                      decoration: InputDecoration(hintText: 'Email'),
                      onSaved: (String value) {
                        customer.email = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      controller: _controllerAddress,
                      decoration: InputDecoration(hintText: 'Address'),
                      onSaved: (String value) {
                        customer.address = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      controller: _controllerPhone,
                      decoration: InputDecoration(hintText: 'Phone'),
                      onSaved: (String value) {
                        customer.phone = value;
                      })),
            ],
          )),
      Step(
          title: const Text('Equipment', style: TextStyle(fontSize: 12)),
          subtitle: const Text('Form', style: TextStyle(fontSize: 10)),
          isActive: true,
          state: StepState.indexed,
          content: Column(
            children: [
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Mark'),
                      onSaved: (String value) {
                        equipment.mark = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Model'),
                      onSaved: (String value) {
                        equipment.model = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Board'),
                      onSaved: (String value) {
                        equipment.board = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Processor'),
                      onSaved: (String value) {
                        equipment.processor = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Hard Disk'),
                      onSaved: (String value) {
                        equipment.hardDisk = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Ram'),
                      onSaved: (String value) {
                        equipment.ram = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Type'),
                      onSaved: (String value) {
                        equipment.type = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Reader CD'),
                      onSaved: (String value) {
                        equipment.readerCd = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Others'),
                      onSaved: (String value) {
                        equipment.other = value;
                      })),
            ],
          )),
      Step(
          title: const Text('Admission', style: TextStyle(fontSize: 12)),
          subtitle: const Text('Form', style: TextStyle(fontSize: 10)),
          isActive: true,
          //state: StepState.editing,
          state: StepState.indexed,
          content: Column(
            children: [
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      maxLines: 4,
                      keyboardType: TextInputType.multiline,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Observation'),
                      onSaved: (String value) {
                        admissionDataSheet.observation = value;
                      })),
              Container(
                  padding: EdgeInsets.only(bottom: 5),
                  child: TextFormField(
                      maxLines: 4,
                      keyboardType: TextInputType.multiline,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(hintText: 'Reason'),
                      onSaved: (String value) {
                        admissionDataSheet.reason = value;
                      })),
              Container(
                child: ElevatedButton(
                  onPressed: () {
                    _submitRegisterForm();
                  },
                  child: Text('Guardar'),
                ),
              )
            ],
          )),
    ];
  }

  void tapped(int step) {
    // ignore: always_declare_return_types
    return setState(() => currStep = step);
  }

  void continued() {
    // ignore: always_declare_return_types
    return currStep < 2 ? setState(() => currStep += 1) : null;
  }

  void cancel() {
    // ignore: always_declare_return_types
    return currStep > 0 ? setState(() => currStep -= 1) : null;
  }

  void _submitRegisterForm() {
    final formRegister = _formKey.currentState;

    if (formRegister.validate()) {
      var edited = false;
      if (customer.id != null) {
        edited = isCustomerEdited();
      }
      formRegister.save();
      admissionDataSheet.customer = customer;
      equipment.status = ConstantsStatus.byAssigning;
      admissionDataSheet.equipment = equipment;
      var now = DateTime.now();
      var formatter = DateFormat('yyyy-MM-dd');
      var formattedDate = formatter.format(now);
      admissionDataSheet.date = formattedDate;
      _admissionDataSheetBloc.create(admissionDataSheet, edited).then(
          (ApiResponse data) {
        if (data.statusResponse == HttpStatus.created) {
          AdmissionDataSheet admission = data.payload;
          TechBusinessNavigator.goToAdmissionDetail(context, admission.id);
          showDialogSuccess(context, 'Guardado exitoso');
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  void _getCustomer(String document) {
    _customerBloc.getCustomerByDocument(document).then((ApiResponse data) {
      if (data.statusResponse == HttpStatus.ok) {
        customer = data.payload;
        _loadDataController();
      } else {
        ErrorApiResponse err = data.payload;
        showAlert(context, err.message);
      }
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  void _loadDataController() {
    _controllerName.text = customer.name;
    _controllerLastName.text = customer.lastName;
    _controllerAddress.text = customer.address;
    _controllerEmail.text = customer.email;
    _controllerPhone.text = customer.phone;
  }

  bool isCustomerEdited() {
    if (_controllerName.text == customer.name) {
      return true;
    }
    if (_controllerLastName.text == customer.lastName) {
      return true;
    }
    if (_controllerAddress.text == customer.address) {
      return true;
    }
    if (_controllerEmail.text == customer.email) {
      return true;
    }
    if (_controllerPhone.text == customer.phone) {
      return true;
    }
    return false;
  }

  void _showAlertDialog() {
    Widget cancel = TextButton(
      onPressed: () {
        Navigator.pop(context);
      },
      child: Text(Constants.btnCancel),
    );

    Widget accept = TextButton(
      onPressed: () {
        TechBusinessNavigator.goToAuthUi(context);
      },
      child: Text(Constants.btnAccept),
    );

    var alert = AlertDialog(
      title: Text(
        Constants.logout,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            Text(Constants.messageLogoutConfirm,
                style: Theme.of(context).textTheme.bodyText2),
          ],
        ),
      ),
      actions: [
        cancel,
        accept,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
