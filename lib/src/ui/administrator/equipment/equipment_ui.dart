import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:multilevel_drawer/multilevel_drawer.dart';
import 'package:techbusinessfrontend/src/bloc/equiment_bloc.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/custom_list_item.dart';

class EquipmentUi extends StatefulWidget {
  const EquipmentUi({Key key}) : super(key: key);

  @override
  EquipmentUiState createState() => EquipmentUiState();
}

class EquipmentUiState extends State<EquipmentUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _textFieldController = TextEditingController();

  EquipmentUiState();
  List<EquipmentUi> lstEquipment = [];
  List<String> dropdownOptionsMenu = Constants.actionsEquipmentMenu;

  Equipment equipment = Equipment();
  EquipmentBLoc equipmentBloc;
  var option;

  @override
  void dispose() {
    super.dispose();
    equipmentBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    equipmentBloc = EquipmentBLoc();
  }

  @override
  Widget build(BuildContext context) {
    option = {'option': 'b'};
    equipmentBloc.initializeData(option);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text(Constants.labelEquipment),
        backgroundColor: ConstantsColors.primaryColor,
      ),
      drawer: MultiLevelDrawer(
        header: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 90,
              ),
              Text('Bienvenido')
            ],
          ),
        ),
        children: [
          MLMenuItem(
              leading: Icon(Icons.account_balance_wallet),
              trailing: Icon(Icons.keyboard_arrow_right),
              subMenuItems: [
                MLSubmenu(
                    submenuContent: Text(Constants.mlSubContentMarkOrder),
                    onClick: () {
                      option = {'option': 'a'};
                      equipmentBloc.initializeData(option);
                    }),
                MLSubmenu(
                    submenuContent: Text(Constants.mlSubContentIdOrder),
                    onClick: () {
                      option = {'option': 'b'};
                      equipmentBloc.initializeData(option);
                    })
              ],
              content: Text(Constants.mlItemContentOrder),
              onClick: () {}),
          MLMenuItem(
              leading: Icon(Icons.account_balance_wallet),
              trailing: Icon(Icons.keyboard_arrow_right),
              subMenuItems: [
                MLSubmenu(
                    submenuContent: Text(Constants.mlSubContentMarkFilter),
                    onClick: () {
                      _alertDialogFilterMark(context);
                    }),
                MLSubmenu(
                    submenuContent: Text(Constants.mlSubContentIdFilter),
                    onClick: () {
                      _alertDialogFilterId(context);
                    })
              ],
              content: Text(Constants.mlItemContentFilter),
              onClick: () {}),
          MLMenuItem(
              leading: Icon(Icons.account_balance_wallet),
              content: Text(Constants.mlItemContentGetAll),
              onClick: () {
                option = {'option': 'b'};

                equipmentBloc.initializeData(option);
              }),
        ],
      ),
      body: _buildStream(),
    );
  }

  Widget _buildStream() {
    return StreamBuilder<List<Equipment>>(
        stream: equipmentBloc.equipmentList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return _buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Future _alertDialogFilterMark(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Ingrese la marca'),
            content: TextField(
              controller: _textFieldController,
            ),
            actions: <Widget>[
              MaterialButton(
                elevation: 5.0,
                onPressed: () {
                  equipment.mark = _textFieldController.text.toUpperCase();
                  option = {
                    'mark': equipment.mark,
                  };
                  equipmentBloc.initializeData(option);

                  _textFieldController.clear();
                },
                child: Text('Aceptar'),
              ),
              MaterialButton(
                elevation: 5.0,
                onPressed: () {
                  _textFieldController.clear();
                  Navigator.of(context).pop();
                },
                child: Text('Cancelar'),
              )
            ],
          );
        });
  }

  Future _alertDialogFilterId(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Ingrese el ID'),
            content: TextField(
              controller: _textFieldController,
            ),
            actions: <Widget>[
              MaterialButton(
                elevation: 5.0,
                onPressed: () {
                  equipment.id = int.parse(_textFieldController.text);
                  option = {
                    'id': equipment.id.toString(),
                  };
                  equipmentBloc.initializeData(option);
                  _textFieldController.clear();
                },
                child: Text('Aceptar'),
              ),
              MaterialButton(
                elevation: 5.0,
                onPressed: () {
                  _textFieldController.clear();
                  Navigator.of(context).pop();
                },
                child: Text('Cancelar'),
              )
            ],
          );
        });
  }

  Widget _buildList(List<Equipment> data) {
    return CustomScrollView(
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return Column(children: [
            CustomListItem(
              title: data[index].model,
              description: data[index].mark,
              status: 'true',
              actionShow: () {
                TechBusinessNavigator.goToEquipmentDetail(context, data[index]);
              },
            ),
            Divider()
          ]);
        }, childCount: data.length))
      ],
    );
  }


}
