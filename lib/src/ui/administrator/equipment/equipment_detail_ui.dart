import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/equiment_bloc.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

class DetailEquipmentUI extends StatefulWidget {
  final Equipment equipment;

  const DetailEquipmentUI({Key key, this.equipment}) : super(key: key);

  @override
  DetailEquipmentUiState createState() => DetailEquipmentUiState();
}

class DetailEquipmentUiState extends State<DetailEquipmentUI>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Equipment equipment;
  EquipmentBLoc equipmentBLoc;
  List<Equipment> option;
  List<String> dropdownOptionsMenu = Constants.actionsChecklistMenu;

  @override
  void dispose() {
    super.dispose();
    equipmentBLoc.dispose();
  }

  @override
  void initState() {
    super.initState();
    equipment = widget.equipment;
    equipmentBLoc = EquipmentBLoc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(Constants.appBarDetailEquipment),
        backgroundColor: ConstantsColors.primaryColor,
      ),
      body: Column(
        children: [
          Expanded(child: Text('id: ' + equipment.id.toString())),
          Expanded(child: Text('model: ' + equipment.model)),
          Expanded(child: Text('mark: ' + equipment.mark)),
          Expanded(child: Text('hardDisk: ' + equipment.hardDisk)),
          Expanded(child: Text('type: ' + equipment.type)),
          Expanded(child: Text('ram: ' + equipment.ram)),
          Expanded(child: Text('processor: ' + equipment.processor)),
          Expanded(child: Text('board: ' + equipment.board)),
          Expanded(child: Text('status: ' + equipment.status))
        ],
      ),
    );
  }
}
