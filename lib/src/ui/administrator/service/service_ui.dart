// ignore: avoid_web_libraries_in_flutter

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/service_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/custom_list_item.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';

class ServiceUI extends StatefulWidget {
  const ServiceUI({Key key}) : super(key: key);

  @override
  ServiceUiState createState() => ServiceUiState();
}

class ServiceUiState extends State<ServiceUI>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formService = GlobalKey<FormState>();

  ServiceUiState();

  List<Service> lstProcess = [];
  Service service = Service();
  ServiceBloc serviceBloc;

  @override
  void dispose() {
    super.dispose();
    serviceBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    serviceBloc = ServiceBloc();
  }

  void _submitDeleteService(Service service) {
    serviceBloc.delete(service).then((ApiResponse data) {
      if (data.statusResponse == HttpStatus.ok) {
        serviceBloc.initializeData();
      } else {
        _showErrorBackend(data);
      }
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  void _submitRegisterForm() {
    final formRegister = _formService.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      _defaultValues();
      serviceBloc.create(service).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.created) {
          serviceBloc.initializeData();
          _closeModal();
        } else {
          _showErrorBackend(data);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  void _defaultValues() {
    service.repairRoute = [];
    service.status = true;
  }

  void _submitUpdateForm() {
    final formRegister = _formService.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      serviceBloc.update(service).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.ok) {
          serviceBloc.initializeData();
          _closeModal();
        } else {
          _showErrorBackend(data);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  void _showErrorBackend(ApiResponse data) {
    ErrorApiResponse err = data.payload;
    showAlert(context, err.message);
  }

  void _closeModal() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    serviceBloc.initializeData();
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text(Constants.labelService),
          backgroundColor: ConstantsColors.primaryColor,
        ),
        body: _buildStream(),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (_) => _buildFormDialog(context, null, true),
              );
            },
            backgroundColor: ConstantsColors.primaryColor,
            child: Icon(
              Icons.add,
              size: 24,
            )));
  }

  Widget _buildStream() {
    return StreamBuilder<List<Service>>(
        stream: serviceBloc.serviceList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget buildList(List<Service> data) {
    return CustomScrollView(
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return Column(children: [
            Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
                child: CustomListItem(
                    title: data[index].name,
                    description: data[index].description,
                    status: data[index].status.toString(),
                    actionDelete: () {
                      _submitDeleteService(data[index]);
                    },
                    actionEdit: () {
                      showDialog(
                        context: context,
                        builder: (_) =>
                            _buildFormDialog(context, data[index], false),
                      );
                    },
                    actionChecklist: () {
                      TechBusinessNavigator.goToServiceDetail(
                          context, data[index]);
                    })),
          ]);
        }, childCount: data.length))
      ],
    );
  }

  Widget _buildFormDialog(
      BuildContext context, Service serviceLoad, bool isNew) {
    isNew ? service = Service() : service = serviceLoad;
    var title = isNew
        ? Constants.labelCreates + ' ' + Constants.labelService.toLowerCase()
        : Constants.labelUpdates + ' ' + Constants.labelService.toLowerCase();
    return AlertDialog(
      title: Center(child: Text(title)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Form(
            key: _formService,
            child: Column(
              children: <Widget>[
                TextFormField(
                  initialValue: isNew ? '' : service.name,
                  maxLength: 50,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  decoration: InputDecoration(hintText: Constants.labelName),
                  onSaved: (String value) {
                    service.name = value;
                  },
                ),
                TextFormField(
                  initialValue: isNew ? '' : service.description,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  maxLines: 2,
                  maxLength: 150,
                  decoration:
                      InputDecoration(hintText: Constants.labelDescription),
                  onSaved: (String value) {
                    service.description = value;
                  },
                ),
              ],
            ),
          )
        ],
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OutlinedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                Constants.btnCancel,
                style: TextStyle(color: ConstantsColors.primaryColor),
              ),
            ),
            isNew
                ? ElevatedButton(
                    onPressed: () {
                      _submitRegisterForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor,
                        onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnCreates))
                : ElevatedButton(
                    onPressed: () {
                      _submitUpdateForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor,
                        onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnUpdates)),
          ],
        )
      ],
    );
  }
}
