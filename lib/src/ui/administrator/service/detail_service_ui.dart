import 'dart:io';

import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/service_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/model/repair_route_model.dart';
import 'package:techbusinessfrontend/src/model/service_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';

class DetailServiceUI extends StatefulWidget {
  final Service service;

  const DetailServiceUI({Key key, this.service}) : super(key: key);

  @override
  DetailServiceUiState createState() => DetailServiceUiState();
}

class DetailServiceUiState extends State<DetailServiceUI>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Service service;
  ServiceBloc serviceBloc;
  List<Process> option;
  List<String> dropdownOptionsMenu = Constants.actionsChecklistMenu;

  @override
  void dispose() {
    super.dispose();
    serviceBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    serviceBloc = ServiceBloc();
    service = widget.service;
    WidgetsBinding.instance.addPostFrameCallback((_) => filterProcess());
  }

  void filterProcess() {
    var listId = service.repairRoute.map((e) => e.process.id);

    serviceBloc.getProcess().then((List<Process> value) {
      value.removeWhere((element) => listId.contains(element.id));
      option = value;
    });
  }

  void _submitUpdate() {
    serviceBloc.update(service).then((ApiResponse data) {
      if (data.statusResponse == HttpStatus.ok) {
        Navigator.of(context).pop();
        showDialogSuccess(context, 'Guardado exitoso');
      } else {
        ErrorApiResponse err = data.payload;
        showAlert(context, err.message);
      }
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  @override
  Widget build(BuildContext context) {
    serviceBloc.initializeDataDetail(service);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Checklist ' + service.name),
        backgroundColor: ConstantsColors.primaryColor,
        actions: [dropdownMenu()],
      ),
      body: _buildStream(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _settingModalBottomSheet(context);
        },
        backgroundColor: ConstantsColors.primaryColor,
        child: Icon(
          Icons.add,
          size: 24,
        ),
      ),
    );
  }

  Widget _buildStream() {
    return StreamBuilder<List<RepairRoute>>(
        stream: serviceBloc.processList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  void _addProcess(Process process) {
    setState(() {
      option.remove(process);
    });
    var check = RepairRoute(process: process);
    service.repairRoute.add(check);
    serviceBloc.initializeDataDetail(service);
  }

  Widget buildList(List<RepairRoute> data) {
    return CustomScrollView(shrinkWrap: true, slivers: <Widget>[
      SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
        return Column(
            children: [_dismissible(context, data[index].process), Divider()]);
      }, childCount: data.length))
    ]);
  }

  Widget dropdown() {
    return DropdownButton<Process>(
      isExpanded: true,
      value: option[0],
      icon: const Icon(Icons.arrow_downward),
      iconSize: 16,
      elevation: 16,
      style: const TextStyle(color: ConstantsColors.primaryColor),
      underline: Container(
        height: 2,
        color: ConstantsColors.secondaryColor,
      ),
      onChanged: (Process processSelect) {
        _addProcess(processSelect);
      },
      items: option.map<DropdownMenuItem<Process>>((Process value) {
        return DropdownMenuItem<Process>(
          value: value,
          child: Text(value.name),
        );
      }).toList(),
    );
  }

  Widget _dismissible(context, item) {
    return Dismissible(
      key: Key(item.name),
      background: _slideLeftBackground(),
      confirmDismiss: (direction) async {
        if (direction == DismissDirection.endToStart) {
          final res = await showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Text(Constants.confirmDelete),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        Constants.btnCancel,
                        style: TextStyle(color: ConstantsColors.black),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        service.repairRoute.remove(item);
                        serviceBloc.initializeDataDetail(service);
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        Constants.actionDelete,
                        style: TextStyle(color: ConstantsColors.error),
                      ),
                    ),
                  ],
                );
              });
          return res;
        } else {
          return null;
        }
      },
      child: InkWell(
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                      flex: 3,
                      child: CheckboxListTile(
                        title: Text(item.name),
                        value: item.status,
                        onChanged: (bool value) {},
                      ))
                ])),
      ),
    );
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
              padding: const EdgeInsets.all(5.0),
              height: 100,
              child: Row(
                children: [
                  Expanded(child: Text(Constants.labelProcess + ' :')),
                  Expanded(child: dropdown()),
                ],
              ));
        });
  }

  Widget dropdownMenu() {
    return DropdownButtonHideUnderline(
      child: DropdownButton<String>(
        isExpanded: false,
        icon: const Icon(
          Icons.more_vert_outlined,
          size: 24,
        ),
        iconSize: 16,
        iconEnabledColor: ConstantsColors.white,
        onChanged: (String optionSelected) {
          _actions(optionSelected);
        },
        items:
            dropdownOptionsMenu.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(StringUtils.capitalize(value)),
          );
        }).toList(),
      ),
    );
  }

  Widget _slideLeftBackground() {
    return Container(
      color: ConstantsColors.error,
      child: Align(
        alignment: Alignment.centerRight,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete,
              color: ConstantsColors.white,
              size: 16,
            ),
            Text(
              Constants.actionDelete,
              style: TextStyle(color: ConstantsColors.white, fontSize: 12),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
      ),
    );
  }

  void _actions(String actionSelected) {
    switch (actionSelected) {
      case Constants.actionDelete:
        {
          service.repairRoute = List.empty();
          _submitUpdate();
        }
        break;

      case Constants.actionSave:
        {
          _submitUpdate();
        }
        break;
    }
  }
}
