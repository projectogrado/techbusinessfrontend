import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

class EmployeeSeachUi extends StatefulWidget {
  const EmployeeSeachUi({Key key}) : super(key: key);

  @override
  EmployeeSeachUiState createState() => EmployeeSeachUiState();
}

class EmployeeSeachUiState extends State<EmployeeSeachUi> with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> _formRegister = GlobalKey<FormState>();
  bool _isVisible = false;

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

/*
  _handleSubmitted() {
    antenaBloc.listarAntena().then((apiResponse) {
      setState(() {
        listAntena = apiResponse.listAntena;
      });
    });
  }
*/
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Buscar empleado por cedula'),
          backgroundColor: Colors.indigo,
          automaticallyImplyLeading: false,
          leading: GestureDetector(
            onTap: () {
              Navigator.pushReplacementNamed(context, Constants.employeeUiRoute);
            },
            child: Icon(
              Icons.arrow_back,
              size: 24,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: _formRegister,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 120.0),
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
                              icon: Icon(Icons.person_search),
                              hintText: 'Cedula'),
                          keyboardType: TextInputType.number,
                          onSaved: (String value) {
                            //employee.phone = value;
                          },
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: ElevatedButton(
                            onPressed: () {
                              showToast();
                              //_submitRegisterForm();
                            },
                            child: Text('Buscar'),
                          ),
                        ),
                        Visibility(
                            visible: _isVisible,
                            child: Card(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                margin: EdgeInsets.all(5),
                                elevation: 5,
                                color: ConstantsColors.veryLightGray,
                                child: Column(children: [
                                  ListTile(
                                    title: Text('employee name'),
                                    subtitle: Text('status = false'),
                                    trailing: Wrap(
                                      children: [
                                        IconButton(
                                            icon: Icon(Icons.edit),
                                            onPressed: () {
                                              /*
                                  showDialog(
                                    context: context,
                                    builder: (_) => _buildPopupDialog(
                                        context, data[index], false),
                                  );
                                  */
                                            }),
                                        Switch(
                                          value: false,
                                          onChanged: (bool value) {},
                                        ),
                                      ],
                                    ),
                                    isThreeLine: true,
                                  )
                                ]))),
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }
}
