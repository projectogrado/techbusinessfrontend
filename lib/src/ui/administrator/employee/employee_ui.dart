import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/employee_bloc.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/custom_list_item.dart';

import '../../../navegator_techbusiness.dart';

class EmployeeListUi extends StatefulWidget {
  @override
  EmployeeListUiState createState() => EmployeeListUiState();
}

class EmployeeListUiState extends State<EmployeeListUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  EmployeeListUiState();

  List<Employee> listEmployee = [];
  Employee employee = Employee();
  EmployeeBloc employeeBloc;

  @override
  void dispose() {
    super.dispose();
    employeeBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    employeeBloc = EmployeeBloc();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => employeeBloc.initializeData());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text(Constants.labelEmployee),
          centerTitle: true,
          backgroundColor: ConstantsColors.primaryColor,
        ),
        body: Stack(fit: StackFit.expand, children: <Widget>[
          _buildStream(),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: FloatingActionButton(
                        onPressed: () {
                          TechBusinessNavigator.goToEmployeeSearchUi(context);
                        },
                        backgroundColor: ConstantsColors.secondaryColor,
                        child: Icon(
                          Icons.search,
                          size: 24,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                    ),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: FloatingActionButton(
                            onPressed: () {
                              _redirectCreate(context);
                            },
                            backgroundColor: ConstantsColors.secondaryColor,
                            child: Icon(Icons.add, size: 24)))
                  ]))
        ]));
  }

  void _redirectCreate(BuildContext context) async {
    final data = await TechBusinessNavigator.goToEmployeeRegisterUi(context);
    await employeeBloc.initializeData();
  }

  Widget _buildStream() {
    return StreamBuilder<List<Employee>>(
        stream: employeeBloc.employeeList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget buildList(List<Employee> data) {
    return CustomScrollView(slivers: [
      SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(children: [
              CustomListItem(
                  title: data[index].name,
                  description: data[index].email,
                  status: '$data[index].status'),
              Divider()
            ]);
          }, childCount: data.length))
    ]);
  }
}
