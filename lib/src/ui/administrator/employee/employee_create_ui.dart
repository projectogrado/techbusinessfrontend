import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:techbusinessfrontend/src/bloc/employee_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/employee_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/navegator_techbusiness.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';

class EmployeeUi extends StatefulWidget {
  const EmployeeUi({Key key}) : super(key: key);

  @override
  EmployeeUiState createState() => EmployeeUiState();
}

class EmployeeUiState extends State<EmployeeUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formRegister = GlobalKey<FormState>();

  bool visibility = true;

  EmployeeUiState();

  EmployeeBloc employeeBloc;
  Employee employee = Employee();

  @override
  void dispose() {
    super.dispose();
    employeeBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    employeeBloc = EmployeeBloc();
  }

  void _submitRegisterForm() {
    final formRegister = _formRegister.currentState;
    if (formRegister.validate()) {
      formRegister.save();
      employeeBloc.create(employee).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.created) {
          TechBusinessNavigator.goBacks(context);
          showDialogSuccess(context, 'Guardado exitoso');
        } else {
          ErrorApiResponse err = data.payload;
          showAlert(context, err.message);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: const Text(Constants.appBarRegisterEmployee),
          centerTitle: true,
          backgroundColor: ConstantsColors.primaryColor,
        ),
        body: SingleChildScrollView(
            child: ConstrainedBox(
          constraints:
              BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Stack(children: [
            Padding(padding: const EdgeInsets.all(8.0), child: _buildForm()),
          ]),
        )));
  }

  Form _buildForm() {
    return Form(
      key: _formRegister,
      child: Column(children: <Widget>[
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.drive_file_rename_outline),
                    hintText: 'Name'),
                onSaved: (String value) {
                  employee.name = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.drive_file_rename_outline),
                    hintText: 'Lastname'),
                onSaved: (String value) {
                  employee.lastName = value;
                })),
        SelectFormField(
            type: SelectFormFieldType.dropdown,
            initialValue: 'CC',
            labelText: 'Type Document ',
            items: Constants.itemSelectTypeDocument,
            onChanged: print,
            onSaved: (String value) {
              employee.typeDocument = value;
            }),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.card_membership),
                    hintText: 'Document'),
                onSaved: (String value) {
                  employee.document = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email_outlined), hintText: 'Email'),
                onSaved: (String value) {
                  employee.email = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.roofing_outlined),
                    hintText: 'Address'),
                onSaved: (String value) {
                  employee.address = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.phone_android), hintText: 'Phone'),
                onSaved: (String value) {
                  employee.phone = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.person), hintText: 'Username'),
                onSaved: (String value) {
                  employee.username = value;
                })),
        Container(
            padding: EdgeInsets.only(bottom: 5),
            child: TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                }
                return null;
              },
              obscureText: visibility,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.vpn_key),
                  hintText: 'Password',
                  suffix: IconButton(
                    icon: Icon(visibility
                        ? Icons.visibility_rounded
                        : Icons.visibility_off_rounded),
                    onPressed: () {
                      setState(() {
                        visibility = !visibility;
                      });
                    },
                  )),
              onSaved: (String value) {
                employee.password = value;
              },
            )),
        SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: ConstantsColors.primaryColor),
              onPressed: () {
                _submitRegisterForm();
              },
              child: Text('Register'),
            ))
      ]),
    );
  }
}
