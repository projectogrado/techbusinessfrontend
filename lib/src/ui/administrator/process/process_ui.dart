import 'dart:io';

import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/process_bloc.dart';
import 'package:techbusinessfrontend/src/model/api_response_model.dart';
import 'package:techbusinessfrontend/src/model/error_api_response_model.dart';
import 'package:techbusinessfrontend/src/model/process_model.dart';
import 'package:techbusinessfrontend/src/utils/widgets/dialog_helper.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';
import 'package:techbusinessfrontend/src/utils/widgets/list_item_description.dart';

class ProcessUI extends StatefulWidget {
  const ProcessUI({Key key}) : super(key: key);

  @override
  ProcessUiState createState() => ProcessUiState();
}

class ProcessUiState extends State<ProcessUI> with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formProcess = GlobalKey<FormState>();

  ProcessUiState();

  List<Process> lstProcess = [];
  Process process = Process();
  ProcessBloc processBloc;

  @override
  void dispose() {
    super.dispose();
    processBloc.dispose();
  }

  @override
  void initState() {
    super.initState();
    processBloc = ProcessBloc();
  }

  void _submitChangeStatus(newStatus, Process process) {
    process.status = newStatus;
    processBloc.update(process).then((ApiResponse data) {
      if (data.statusResponse == HttpStatus.ok) {
        processBloc.initializeData();
      } else {
        _showErrorBackend(data);
      }
    }, onError: (err) {
      showAlert(context, err.message);
    });
  }

  void _showErrorBackend(ApiResponse data) {
    ErrorApiResponse err = data.payload;
    showAlert(context, err.message);
  }

  void _submitRegisterForm() {
    final formRegister = _formProcess.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      process.status = true;
      processBloc.create(process).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.created) {
          processBloc.initializeData();
          _closeModal();
        } else {
          _showErrorBackend(data);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  void _closeModal() {
    Navigator.of(context).pop();
  }

  void _submitUpdateForm() {
    final formRegister = _formProcess.currentState;

    if (formRegister.validate()) {
      formRegister.save();
      processBloc.update(process).then((ApiResponse data) {
        if (data.statusResponse == HttpStatus.ok) {
          processBloc.initializeData();
          _closeModal();
        } else {
          _showErrorBackend(data);
        }
      }, onError: (err) {
        showAlert(context, err.message);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    processBloc.initializeData();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text(Constants.labelProcess),
        backgroundColor: ConstantsColors.primaryColor,
      ),
      body: _buildStream(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (_) => _buildFormDialog(context, null, true),
          );
        },
        backgroundColor: ConstantsColors.secondaryColor,
        child: Icon(
          Icons.add,
          size: 24,
        ),
      ),
    );
  }

  Widget _buildStream() {
    return StreamBuilder<List<Process>>(
        stream: processBloc.processList,
        builder: (BuildContext context, AsyncSnapshot snapShot) {
          if (snapShot.hasData) {
            return buildList(snapShot.data);
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  Widget buildList(List<Process> data) {
    return CustomScrollView(
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return Column(children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: ListItemDescription(
                      title: StringUtils.capitalize(data[index].name),
                      description: StringUtils.capitalize(data[index].description),
                      status: data[index].status.toString(),
                    ),
                  ),
                  Wrap(
                    children: [
                      IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (_) => _buildFormDialog(context, data[index], false),
                          );
                        },
                      ),
                      Switch(
                        value: data[index].status,
                        onChanged: (bool newValue) {
                          _submitChangeStatus(newValue, data[index]);
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Divider()
          ]);
        }, childCount: data.length))
      ],
    );
  }

  Widget _buildFormDialog(BuildContext context, Process processLoad, bool isNew) {
    isNew ? process = Process() : process = processLoad;
    var title = isNew
        ? Constants.labelCreates + ' ' + Constants.labelProcess.toLowerCase()
        : Constants.labelUpdates + ' ' + Constants.labelProcess.toLowerCase();
    return AlertDialog(
      title: Center(child: Text(title)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Form(
            key: _formProcess,
            child: Column(
              children: <Widget>[
                TextFormField(
                  initialValue: isNew ? '' : process.name,
                  maxLength: 50,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  decoration: InputDecoration(hintText: Constants.labelName),
                  onSaved: (String value) {
                    process.name = value;
                  },
                ),
                TextFormField(
                  initialValue: isNew ? '' : process.description,
                  validator: (value) {
                    if (value.isEmpty) {
                      return Constants.someText;
                    }
                    return null;
                  },
                  maxLines: 2,
                  maxLength: 150,
                  decoration: InputDecoration(hintText: Constants.labelDescription),
                  onSaved: (String value) {
                    process.description = value;
                  },
                ),
              ],
            ),
          )
        ],
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            OutlinedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                Constants.btnCancel,
                style: TextStyle(color: ConstantsColors.primaryColor),
              ),
            ),
            isNew
                ? ElevatedButton(
                    onPressed: () {
                      _submitRegisterForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor, onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnCreates))
                : ElevatedButton(
                    onPressed: () {
                      _submitUpdateForm();
                    },
                    style: ElevatedButton.styleFrom(
                        primary: ConstantsColors.primaryColor, onPrimary: ConstantsColors.white),
                    child: Text(Constants.btnUpdates)),
          ],
        )
      ],
    );
  }
}
