import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

import '../../navegator_techbusiness.dart';

class AdministratorUi extends StatefulWidget {
  @override
  AdministratorUiState createState() => AdministratorUiState();
}

class AdministratorUiState extends State<AdministratorUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var mediaQueryData = MediaQuery.of(context);
    final widthScreen = mediaQueryData.size.width;
    final appBarHeight = kToolbarHeight;
    final paddingBottom = mediaQueryData.padding.bottom;
    final heightScreen =
        mediaQueryData.size.height - paddingBottom - appBarHeight;

    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            title: const Text(Constants.appBarMenu),
            automaticallyImplyLeading: false,
            actions: [
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: GestureDetector(
                        onTap: () {
                          _showConfirmDialog();
                        },
                        child: Icon(
                          Icons.logout,
                          size: 24,
                        )),
                  )
                ],
              )
            ]),

        // drawer: menu,
        body: SafeArea(
            child: GridView.count(
          padding: EdgeInsets.all(16.0),
          crossAxisCount: 2,
          crossAxisSpacing: 6,
          mainAxisSpacing: 6,
          childAspectRatio: widthScreen / (heightScreen / 1.55),
          children: [
            _showItemGridMenu(
                Constants.employeeUiRoute,
                Constants.menuTitleEmployee,
                Icons.account_circle,
                ConstantsColors.moderateGray),
            _showItemGridMenu(
                Constants.customerUiRoute,
                Constants.menuTitleCustomer,
                Icons.group,
                ConstantsColors.moderateGray),
            _showItemGridMenu(
                Constants.serviceUiRoute,
                Constants.menuTitleService,
                Icons.assignment_turned_in_rounded,
                ConstantsColors.moderateGray),
            _showItemGridMenu(
                Constants.processUiRoute,
                Constants.menuTitleProcess,
                Icons.view_list_rounded,
                ConstantsColors.moderateGray),
            _showItemGridMenu(
                Constants.equipmentUiRoute,
                Constants.menuTitleEquipment,
                Icons.monitor,
                ConstantsColors.moderateGray),
            _showItemGridMenu(
                Constants.configUiRoute,
                Constants.menuTitleConfig,
                Icons.construction_outlined,
                ConstantsColors.moderateGray),
          ],
        )));
  }

  Widget _showItemGridMenu(route, text, icon, color) {
    return InkWell(
        onTap: () {
          Navigator.pushNamed(context, route);
        },
        child: Card(
          elevation: 24,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                  child: Icon(
                icon,
                size: 48,
                color: color,
              )),
              Center(
                child: Text(text),
              )
            ],
          ),
        ));
  }

  void _showConfirmDialog() {
    Widget cancel = TextButton(
      onPressed: () {
        Navigator.pop(context);
      },
      child: Text(Constants.btnCancel),
    );

    Widget accept = TextButton(
      onPressed: () {
        TechBusinessNavigator.goToAuthUi(context);
      },
      child: Text(Constants.btnAccept),
    );

    var alert = AlertDialog(
      title: Text(
        Constants.logout,
        style: Theme.of(context).textTheme.bodyText1,
      ),
      content: SingleChildScrollView(
        child: ListBody(
          children: [
            Text(Constants.messageLogoutConfirm,
                style: Theme.of(context).textTheme.bodyText2),
          ],
        ),
      ),
      actions: [
        cancel,
        accept,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
