import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:techbusinessfrontend/src/bloc/customer_bloc.dart';
import 'package:techbusinessfrontend/src/model/customer_model.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants.dart';
import 'package:techbusinessfrontend/src/utils/Constants/constants_color.dart';

class CustomerUi extends StatefulWidget {
  const CustomerUi({Key key}) : super(key: key);

  @override
  CustomerUiState createState() => CustomerUiState();
}

class CustomerUiState extends State<CustomerUi>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController editingController = TextEditingController();
  List<Customer> lstProcess = [];
  Customer customer = Customer();
  CustomerBloc customerBloc;
  var items;

  void _handleSubmitted() {
    customerBloc.initializeData().then((apiResponse) {
      setState(() {
        lstProcess = apiResponse.payload;
        items = <Customer>[];
        items.addAll(lstProcess);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    customerBloc = CustomerBloc();
    _handleSubmitted();
  }

  void filterSearchResults(String query) {
    var dummySearchList = <Customer>[];
    dummySearchList.addAll(lstProcess);
    if (query.isNotEmpty) {
      var dummyListData = <Customer>[];
      dummySearchList.forEach((item) {
        if (item.document.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        // items.clear();
        items.addAll(lstProcess);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    customerBloc.initializeData();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text(Constants.labelCustomer),
        backgroundColor: ConstantsColors.primaryColor,
      ),
      body: Stack(fit: StackFit.expand, children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  onChanged: (value) {
                    filterSearchResults(value);
                  },
                  controller: editingController,
                  decoration: InputDecoration(
                      labelText: Constants.labelSearchDocument,
                      hintText: Constants.hintSearchDocument,
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(25.0)))),
                ),
              ),
              if (items != null)
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: items.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        elevation: 50,
                        shadowColor: Colors.black,
                        child: Container(
                          padding: EdgeInsets.all(32.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Customer',
                                style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ),
                                //Textstyle
                              ),
                              Text(
                                Constants.cardName + items[index].name,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                              Text(
                                Constants.cardLastName + items[index].lastName,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                              Text(
                                Constants.cardDocument + items[index].document,

                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                              Text(
                                Constants.cardEmail + items[index].email,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                              Text(
                                Constants.cardAddresses + items[index].address,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                              Text(
                                Constants.cardCellPhone + items[index].phone,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.green[900],
                                  fontWeight: FontWeight.w500,
                                ), //Textstyle
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
            ],
          ),
        ),
      ]),
    );
  }
}
