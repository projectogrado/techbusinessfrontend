import 'package:flutter/cupertino.dart';
import 'package:techbusinessfrontend/src/model/discharge_data_sheet_model.dart';
import 'package:techbusinessfrontend/src/model/equiment_model.dart';
import 'package:techbusinessfrontend/src/model/work_service_model.dart';
import 'package:techbusinessfrontend/src/utils/token_helper.dart';

import 'model/service_model.dart';
import 'utils/Constants/constants.dart';

class TechBusinessNavigator {
  static void goToAdministratorHomeUi(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Constants.administratorHomeRoute, (Route<dynamic> route) => false);
  }

  static void goToAuthUi(BuildContext context) {
    final token = TokenHelper();
    token.logout();
    Navigator.of(context).pushNamedAndRemoveUntil(
        Constants.authUiRoute, (Route<dynamic> route) => false);
  }

  static Future<void> goToEmployeeRegisterUi(BuildContext context)  async {
    await Navigator.pushNamed(context, Constants.employeeCreateUiRoute);
  }

  static void goToProcessUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.configUiRoute);
  }

  static void goToEmployeeSearchUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.employeeSearchUiRoute);
  }

  static void goToEmployeeListUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.employeeUiRoute);
  }

  static void goToCustomerListUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.customerUiRoute);
  }

  static void goToEmployeeHomeUi(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Constants.employeeHomeUiRoute, (Route<dynamic> route) => false);
  }

  static void goToDeliveryPerson(
      BuildContext context, DischargeDataSheet dischargeDataSheet) {
    Navigator.pushNamed(context, Constants.deliveryPersonRoute,
        arguments: {Constants.labelDischargeDataSheet: dischargeDataSheet});
  }

  static void goToRouteWork(BuildContext context, int id) {
    Navigator.pushNamed(context, Constants.routeWorkRoute,
        arguments: {Constants.labelWorkService: id});
  }

  // Menu Employee
  static void goToAdmissionDataSheetUi(BuildContext context) {
    Navigator.pushNamed(context, Constants.employeeAdmissionUiRoute);
  }

  static void goToDelivery(BuildContext context) {
    Navigator.pushNamed(context, Constants.employeeHomeUiRoute);
  }

  static void goToReaderQr(BuildContext context) {
    Navigator.pushNamed(context, Constants.employeeReaderQrUiRoute);
  }

  static void goToDischargeDataSheet(
      BuildContext context, DischargeDataSheet dischargeDataSheet) {
    Navigator.pushNamed(context, Constants.employeeDischargeUiRoute,
        arguments: {Constants.labelDischargeDataSheet: dischargeDataSheet});
  }

  static void goToResetPassword(
      BuildContext context, String username, String rol) {
    Navigator.pushNamed(context, Constants.resetPasswordUiRoute, arguments: {
      Constants.labelUsername: username,
      Constants.labelRol: rol
    });
  }

  static void goToServiceDetail(BuildContext context, Service service) {
    Navigator.pushNamed(context, Constants.serviceDetailUiRoute,
        arguments: {Constants.labelService: service});
  }

  static void goToAdmissionDetail(BuildContext context, int id) {
    Navigator.pushNamed(context, Constants.admissionUiRoute,
        arguments: {Constants.labelId: id});
  }

  static void goToRouteWorkDetail(
      BuildContext context, WorkService workService, int id) {
    Navigator.pushNamed(context, Constants.routeWorkDetailRoute, arguments: {
      Constants.labelService: workService,
      Constants.labelId: id
    });
  }

  static void goToEquipmentDetail(BuildContext context, Equipment equipment) {
    Navigator.pushNamed(context, Constants.equipmentDetailUiRoute,
        arguments: {Constants.labelEquipment: equipment});
  }

  static void goBack(BuildContext context) {
    Navigator.pop(context);
  }

  static void goBacks(BuildContext context) {
    Navigator.of(context).pop();
  }

  static void goEmployeeHome(BuildContext context) {
    Navigator.of(context).popUntil((route) => route.isFirst);
  }
}
